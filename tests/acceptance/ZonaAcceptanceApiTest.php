<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ZonaAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->zona = factory(App\Models\Zona::class)->make([
            'id' => '1',
		'spam_id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $this->zonaEdited = factory(App\Models\Zona::class)->make([
            'id' => '1',
		'spam_id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $user = factory(App\Repositories\User\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/zonas');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/zonas', $this->zona->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/zonas', $this->zona->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/zonas/1', $this->zonaEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('zonas', $this->zonaEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/zonas', $this->zona->toArray());
        $response = $this->call('DELETE', 'api/v1/zonas/'.$this->zona->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'zona was deleted']);
    }

}
