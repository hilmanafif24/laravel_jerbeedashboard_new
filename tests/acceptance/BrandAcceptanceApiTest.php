<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BrandAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->brand = factory(App\Models\Brand::class)->make([
            'id' => '1',
		'name' => 'laravel',

        ]);
        $this->brandEdited = factory(App\Models\Brand::class)->make([
            'id' => '1',
		'name' => 'laravel',

        ]);
        $user = factory(App\Repositories\User\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/brands');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/brands', $this->brand->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/brands', $this->brand->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/brands/1', $this->brandEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('brands', $this->brandEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/brands', $this->brand->toArray());
        $response = $this->call('DELETE', 'api/v1/brands/'.$this->brand->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'brand was deleted']);
    }

}
