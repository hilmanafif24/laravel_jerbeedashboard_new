<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SpamAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->spam = factory(_namespace_repository_\Spam::class)->make([
            'id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $this->spamEdited = factory(_namespace_repository_\Spam::class)->make([
            'id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $user = factory(App\Repositories\User\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'spams');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('spams');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'spams/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'spams', $this->spam->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('spams/'.$this->spam->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'spams', $this->spam->toArray());

        $response = $this->actor->call('GET', '/spams/'.$this->spam->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('spam');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'spams', $this->spam->toArray());
        $response = $this->actor->call('PATCH', 'spams/1', $this->spamEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('spams', $this->spamEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'spams', $this->spam->toArray());

        $response = $this->call('DELETE', 'spams/'.$this->spam->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('spams');
    }

}
