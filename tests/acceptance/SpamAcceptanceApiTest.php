<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SpamAcceptanceApiTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->spam = factory(App\Models\Spam::class)->make([
            'id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $this->spamEdited = factory(App\Models\Spam::class)->make([
            'id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $user = factory(App\Repositories\User\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'api/v1/spams');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'api/v1/spams', $this->spam->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['id' => 1]);
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'api/v1/spams', $this->spam->toArray());
        $response = $this->actor->call('PATCH', 'api/v1/spams/1', $this->spamEdited->toArray());
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeInDatabase('spams', $this->spamEdited->toArray());
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'api/v1/spams', $this->spam->toArray());
        $response = $this->call('DELETE', 'api/v1/spams/'.$this->spam->id);
        $this->assertEquals(200, $response->getStatusCode());
        $this->seeJson(['success' => 'spam was deleted']);
    }

}
