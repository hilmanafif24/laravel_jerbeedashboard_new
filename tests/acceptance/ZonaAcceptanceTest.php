<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class ZonaAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->zona = factory(_namespace_repository_\Zona::class)->make([
            'id' => '1',
		'spam_id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $this->zonaEdited = factory(_namespace_repository_\Zona::class)->make([
            'id' => '1',
		'spam_id' => '1',
		'kode' => 'laravel',
		'name' => 'laravel',
		'polygon' => 'laravel',

        ]);
        $user = factory(App\Repositories\User\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'zonas');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('zonas');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'zonas/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'zonas', $this->zona->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('zonas/'.$this->zona->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'zonas', $this->zona->toArray());

        $response = $this->actor->call('GET', '/zonas/'.$this->zona->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('zona');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'zonas', $this->zona->toArray());
        $response = $this->actor->call('PATCH', 'zonas/1', $this->zonaEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('zonas', $this->zonaEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'zonas', $this->zona->toArray());

        $response = $this->call('DELETE', 'zonas/'.$this->zona->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('zonas');
    }

}
