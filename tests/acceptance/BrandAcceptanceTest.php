<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class BrandAcceptanceTest extends TestCase
{
    use DatabaseMigrations;
    use WithoutMiddleware;

    public function setUp()
    {
        parent::setUp();

        $this->brand = factory(_namespace_repository_\Brand::class)->make([
            'id' => '1',
		'name' => 'laravel',

        ]);
        $this->brandEdited = factory(_namespace_repository_\Brand::class)->make([
            'id' => '1',
		'name' => 'laravel',

        ]);
        $user = factory(App\Repositories\User\User::class)->make();
        $this->actor = $this->actingAs($user);
    }

    public function testIndex()
    {
        $response = $this->actor->call('GET', 'brands');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('brands');
    }

    public function testCreate()
    {
        $response = $this->actor->call('GET', 'brands/create');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testStore()
    {
        $response = $this->actor->call('POST', 'brands', $this->brand->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('brands/'.$this->brand->id.'/edit');
    }

    public function testEdit()
    {
        $this->actor->call('POST', 'brands', $this->brand->toArray());

        $response = $this->actor->call('GET', '/brands/'.$this->brand->id.'/edit');
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertViewHas('brand');
    }

    public function testUpdate()
    {
        $this->actor->call('POST', 'brands', $this->brand->toArray());
        $response = $this->actor->call('PATCH', 'brands/1', $this->brandEdited->toArray());

        $this->assertEquals(302, $response->getStatusCode());
        $this->seeInDatabase('brands', $this->brandEdited->toArray());
        $this->assertRedirectedTo('/');
    }

    public function testDelete()
    {
        $this->actor->call('POST', 'brands', $this->brand->toArray());

        $response = $this->call('DELETE', 'brands/'.$this->brand->id);
        $this->assertEquals(302, $response->getStatusCode());
        $this->assertRedirectedTo('brands');
    }

}
