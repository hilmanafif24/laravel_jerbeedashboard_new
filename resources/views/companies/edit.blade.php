@extends('dashboard')

@section('content')

  <div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
      <h1 class="pull-left"><span class="fa fa-building"></span> Pengaturan Umum</h1>
    </div>
  </div>

  <div class="row raw-margin-top-24">
      <div class="col-md-6 raw-margin-bottom-24">
          <div>
              {!! Form::model($company, ['route' => ['companies.update', $company->id], 'method' => 'patch']) !!}
                  {!! csrf_field() !!}
                  {!! method_field('PATCH') !!}

                  @form_maker_object($company, FormMaker::getTableColumns('companies'))

                  <div class="raw-margin-top-24">
                      <a class="btn btn-default" href="{{ URL::previous() }}">Cancel</a>
                      <button class="btn btn-primary pull-right" type="submit">Save</button>
                  </div>

            {!! Form::close() !!}
          </div>
      </div>
  </div>

@stop
