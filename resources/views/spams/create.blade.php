@extends('dashboard')

@section('content')
<header>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBNeEuP2m5QWfpG5jANn3SS2Iq3m1a8AlM&libraries=geometry,drawing"></script>
 <script>
     var geocoder; 
     var map;
     var polygonArray = [];
     function initialize() {
      map = new google.maps.Map(
       document.getElementById("map_canvas"), {
        center: new google.maps.LatLng(-6.956273, 107.546515),
        zoom: 9,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });
      var drawingManager = new google.maps.drawing.DrawingManager({
        drawingMode: google.maps.drawing.OverlayType.POLYGON,
        drawingControl: true,
        drawingControlOptions: {
         position: google.maps.ControlPosition.TOP_CENTER,
         drawingModes: [
         google.maps.drawing.OverlayType.POLYGON,
         ]
     },
     polygonOptions: {
        fillColor: '#BCDCF9',
        fillOpacity: 0.5,
        strokeWeight: 2,
        strokeColor: '#57ACF9',
        clickable: false,
        editable: false,
        zIndex: 1
    }
});
      console.log(drawingManager)
      drawingManager.setMap(map)
      google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
        var dat;
        for (var i = 0; i < polygon.getPath().getLength(); i++) {
         dat += polygon.getPath().getAt(i).toUrlValue(6) + ";";
     }
     document.getElementById('polygon').value=dat.replace('undefined','');
     polygonArray.push(polygon);
 });

  }
  google.maps.event.addDomListener(window, "load", initialize);
</script>
</header>
<div class="row">
    <div class="col-md-12" style="width: 98.8%;background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-map-o"></span> Tambah SPAM</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-6 raw-margin-bottom-24">

        {!! Form::open(['route' => 'spams.store']) !!}
        {!! csrf_field() !!}

        @form_maker_table("spams",[
            'id'=>['type'=>'hidden'],
            'polygon'=>['type'=>'hidden'],
            'kode'=>['alt_name'=>'Kode'],
            'name'=>['alt_name'=>'Nama'],
            ])

            <div class="form-group polygon">
                <label for="polygon">Polygon</label>
                <input class="form-control" type="text" name="polygon" id="polygon" required placeholder="Polygon">
            </div>
                <div id="map_canvas" style=" border: 2px solid #3872ac;">
            </div>

            <div class="raw-margin-top-24">
                <a class="btn btn-default pull-left" href="{!! route('spams.index') !!}">Batal</a>
                <button class="btn btn-primary pull-right" type="submit">Buat</button>
            </div>

            {!! Form::close() !!}
        </div>

    </div>
</div>

@stop
