@extends('dashboard')

@section('content')
<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-mobile"></span> Tambah Perangkat</h1>
    </div>
</div>
<div class="row">
    <div class="col-md-6 raw-margin-bottom-24">
        {!! Form::open(['route' => 'devices.store']) !!}
        {!! csrf_field() !!}

        <div class="raw-margin-top-24">
            @input_maker_label('Nama')
            @input_maker_create('name', ['type' => 'string'])
        </div>

        <div class="raw-margin-top-24">
            @input_maker_label('Logger')
            @input_maker_create('station_id', ['type' => 'relationship', 'model' => 'App\Models\Station', 'label' => 'name', 'value' => 'id' ])
        </div>

        <div class="raw-margin-top-24">
            @input_maker_label('Merek')
            @input_maker_create('brand_id', ['type' => 'relationship', 'model' => 'App\Models\Brand', 'label' => 'name', 'value' => 'id' ])
        </div>
        <div class="raw-margin-top-24">
            @input_maker_label('Label')
            @input_maker_create('label', ['type' => 'string'])
        </div>

        <div class="raw-margin-top-24">
            @input_maker_label('Unit')
            @input_maker_create('unit', ['type' => 'string'])
        </div>


        <div class="raw-margin-top-24">
            <a class="btn btn-default pull-left" href="{!! route('devices.index') !!}">Batal</a>
            <button class="btn btn-primary pull-right" type="submit">Buat</button>
        </div>

        {!! Form::close() !!}
    </div>

</div>
</div>

@stop
