@extends('dashboard')

@section('content')

 <div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        {!! Form::open(['route' => 'devices.search','class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
          {!! csrf_field() !!}
          <input class="form-control form-inline pull-right" name="search" placeholder="Search">
        {!! Form::close() !!}
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('devices.create') !!}">Tambah Baru</a>
        <h1 class="pull-left"><span class="fa fa-mobile"></span> Kelola Perangkat</h1>
    </div>
</div>
<div class="row raw-margin-top-24">
    <div class="col-md-12">
        @if($devices->isEmpty())
        <div class="well text-center">Perangkat Tidak Di Temukan.</div>
        @else
        <table class="table table-striped">
            <thead>
                <th>Name</th>
                <th width="130px" class="text-right">Action</th>
            </thead>
            <tbody>
                @foreach($devices as $device)
                <tr>
                    <td>
                        {{ $device->name }}
                    </td>
                    <td>
                        <form method="post" action="{!! route('devices.destroy', [$device->id]) !!}">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this device?')"><i class="fa fa-trash"></i> Delete</button>
                        </form>
                        {{-- <a class="btn btn-warning btn-xs" href="{!! route('devices.show', [$device->id]) !!}"><i class="fa fa-search"></i> Show</a> --}}
                        <a class="btn btn-warning btn-xs" href="{!! route('devices.edit', [$device->id]) !!}"><i class="fa fa-pencil"></i> Edit</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="row text-center">
            {!! $devices; !!}
        </div>
        @endif
    </div>
</div>

@stop
