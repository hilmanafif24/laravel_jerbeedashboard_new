@extends('dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12">
        
        <h1>{{ $device->name }}</h1> 

        <table class="table table-striped">
            <tr>
                <th>Station</th>
                <td>{{ $device->station_id }}</td>
            </tr>

            <tr>
                <th>Brand</th>
                <td>{{ $device->brand_id }}</td>
            </tr>

            <tr>
                <th>Measurement</th>
                <td>{{ $device->measurement_id }}</td>
            </tr>

            <tr>
                <th>Label</th>
                <td>{{ $device->label }}</td>
            </tr>

            <tr>
                <th>Unit</th>
                <td>{{ $device->unit }}</td>
            </tr>

        </table>

        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        Modify this page for detailed show/info.
        </div>
    </div>
    {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-primary pull-right']) !!}
@stop
