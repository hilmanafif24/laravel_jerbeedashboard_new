@extends('dashboard')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ url('css/jquery.datetimepicker.min.css') }}">
@stop

@section('javascript')
<script src="{{ url('js/jquery.datetimepicker.full.min.js') }}"></script>
<script>
    jQuery('#instalation_date').datetimepicker({
      format: 'Y-m-d H:i'
  });
</script>
@stop

@section('content')
<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-tachometer"></span> Tambah Logger</h1>
    </div>
</div>
<div class="row raw-margin-top-24">
    <div class="col-md-6 raw-margin-bottom-24">
        {!! Form::open(['route' => 'stations.store']) !!}
        {!! csrf_field() !!}
        @form_maker_table("stations",[
            'id'=>['type'=>'hidden'],
            'brand_id'=>['alt_name'=>'Merek','type'=>'relationship','model' => 'App\Models\Brand','label'=>'name','value'=>'id'],
            'location_id'=>['alt_name'=>'Posisi','type'=>'relationship','model' => 'App\Models\Location','label'=>'name','value'=>'id'],        
            'name'=>['alt_name'=>'Nama'],
            'lat'=>['alt_name'=>'Latitude'],
            'long'=>['alt_name'=>'Longitude'],
            'spam_id'=>['alt_name'=>'Spam','type'=>'relationship',
            'model' => 'App\Models\Spam','label'=>'name','value'=>'id'],
            'zona_id'=>['alt_name'=>'Zona','type'=>'relationship',
            'model' => 'App\Models\Zona','label'=>'name','value'=>'id'],
            'dma_id'=>['alt_name'=>'Dma','type'=>'relationship',
            'model' => 'App\Models\Dma','label'=>'name','value'=>'id'],
            'v_spam_kode_id'=>['type'=>'hidden'],
            'kode'=>['alt_name'=>'Kode'],
            'chart_url'=>['alt_name'=>'Chart Url'],
            'data_url'=>['alt_name'=>'Data Url'],      
            ])

            <div class="form-group">
                <label class="control-label">Tanggal Instalasi</label>

                <input type="datetime-local" class="form-control" name="instalation_date" value="instalation_date" id="">
            </div>
            <div class="raw-margin-top-24">
                <a class="btn btn-default pull-left" href="{!! route('stations.index') !!}">Batal</a>
                <button class="btn btn-primary pull-right" type="submit">Buat</button>
            </div>

            {!! Form::close() !!}
        </div>
        <div class="col-md-6 raw-margin-bottom-24">
            <div class="form-group">
              <label class="control-label" for="avatar">
              Peta Area</label><br />(Drag marker untuk mengatur input koordinat)
              <div style="width: 300px; height: 300px; border:1px solid #ccc;">
                {!! Mapper::render() !!}
            </div>
        </div>
    </div>

</div>
</div>

@stop
