@extends('dashboard')

@section('stylesheets')
<link rel="stylesheet" type="text/css" href="{{ url('css/jquery.datetimepicker.min.css') }}">
@stop

@section('javascript')
<script src="{{ url('js/jquery.datetimepicker.full.min.js') }}"></script>
<script>
  jQuery('#instalation_date').datetimepicker({
    format: 'Y-m-d H:i'
  });
  $('*[name=instalation_date]').val("{{$station->instalation_date}}");
</script>
@stop
@section('content')
<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-tachometer"></span> Ubah Logger</h1>
    </div>
</div>
<div class="row">
  <div class="col-md-6 raw-margin-bottom-24">
    <div>
      {!! Form::model($station, ['route' => ['stations.update', $station->id], 'method' => 'patch']) !!}
      {!! csrf_field() !!}
      {!! method_field('PATCH') !!}
    <input type="hidden" name="id" value="{{ $station->id }}">
    <div class="raw-margin-top-24">
      @input_maker_label('Merek')
      @input_maker_create('devices', [
          'type' => 'relationship',
          'model' => 'App\Models\Brand',
          'label' => 'name',
          'value' => 'id'
          ],  $station)      
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Posisi')
      @input_maker_create('location_id', ['type' => 'string'],$station)
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Nama')
      @input_maker_create('name', ['type' => 'string'],$station)
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Latitude')
      @input_maker_create('lat', ['type' => 'string'],$station)
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Longitude')
      @input_maker_create('long', ['type' => 'string'],$station)
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Spam')
      @input_maker_create('spam', [
          'type' => 'relationship',
          'model' => 'App\Models\Spam',
          'label' => 'name',
          'value' => 'id'
          ], $station)
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Zona')
      @input_maker_create('zona', [
          'type' => 'relationship',
          'model' => 'App\Models\Zona',
          'label' => 'name',
          'value' => 'id'
          ],$station)
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Dma')
      @input_maker_create('dma', [
          'type' => 'relationship',
          'model' => 'App\Models\Dma',
          'label' => 'name',
          'value' => 'id'
          ],$station)
    </div>
    <input type="hidden" name="id" value="{{ $station->v_spam_kode_id }}">
    <div class="raw-margin-top-24">
      @input_maker_label('Kode')
      @input_maker_create('kode', ['type' => 'string'],$station)      
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Chart Url')
      @input_maker_create('chart_url', ['type' => 'string'],$station)
    </div>
    <div class="raw-margin-top-24">
      @input_maker_label('Data Url')
      @input_maker_create('data_url',['type' => 'string'],$station)
    </div>
    <br>
        <div class="form-group">
          <label class="control-label">Tanggal Instalasi</label>
          <input type="text" class="form-control" name="instalation_date" value="" id="instalation_date">
        </div>
        <div class="raw-margin-top-24">
          <a class="btn btn-default pull-left" href="{!! route('stations.index') !!}">Batal</a>
          <button class="btn btn-primary pull-right" type="submit">Simpan</button>
        </div>

        {!! Form::close() !!}
      </div>
  </div>

  <div class="col-md-6 raw-margin-bottom-24">
            <div class="form-group">
                  <label class="control-label" for="avatar">Peta Area</label><br />(Drag marker untuk mengatur input koordinat)
                  <div style="width: 300px; height: 300px; border:1px solid #ccc;">
                    {!! Mapper::render() !!}
                  </div>
                </div>
       </div>
      </div> 

  @stop