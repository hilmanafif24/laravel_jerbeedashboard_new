@extends('dashboard')
 
@section('content')

<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-flag-o"></span> Ubah Merek</h1>
    </div>
</div>
<div class="row raw-margin-top-24">
      <div class="col-md-6 raw-margin-bottom-24">
          <div>
              {!! Form::model($brand, ['route' => ['brands.update', $brand->id], 'method' => 'patch']) !!}
                  {!! csrf_field() !!}
                  {!! method_field('PATCH') !!}

                  @form_maker_object($brand, [
                    'name' => ['alt_name'=>'Nama'],
                    'phone' => ['alt_name'=>'Telepon'],
                    'address' => ['alt_name'=>'Alamat'],
                    
                  ])

                  <div class="raw-margin-top-24">
                      <img class="img-thumbnail img-circle" src="{{ url($brand->logo->url('small')) }}" style="height: 90px;width: 90px;">
                      <input type="file" name="foto">
                      <br>

                  </div>    

                  <div class="raw-margin-top-24">
                      <a class="btn btn-default pull-left" href="{!! route('brands.index') !!}">Batal</a>
                      <button class="btn btn-primary pull-right" type="submit">Simpan</button>

            {!! Form::close() !!}
          </div>
      </div>
  </div>
</div>

@stop
