@extends('dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12">
            <h1>{{ $brand->name }}</h1>

            <table class="table table-striped">
                <tr>
                    <th>Telepon</th>
                    <td>{{ $brand->phone }}</td>
                </tr>

                <tr>
                    <th>Alamat</th>
                    <td>{{ $brand->address }}</td>
                </tr>

                <tr>
                    <th>Logo</th>
                    <td><img class="img-thumbnail img-circle" src="{{ url($brand->logo->url('small')) }}" style="width: 80px;height: 80px;"></td>
                </tr>

            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
        Modify this page for detailed show/info.
        </div>
    </div>
    {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default btn-primary pull-right']) !!}
@stop