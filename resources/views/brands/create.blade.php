@extends('dashboard')

@section('content')

<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-flag-o"></span> Tambah Merek</h1>
    </div>
</div>
<div class="row raw-margin-top-24">
    <div class="col-md-6 raw-margin-bottom-24">

            {!! Form::open(['route' => 'brands.store', 'files' => true]) !!}
                {!! csrf_field() !!}

                @form_maker_table("brands",[
                    'name' => ['alt_name' => 'Nama'],
                    'phone'=>['alt_name' => 'Telepon'],
                    'address'=>['alt_name' => 'Alamat'],
                    'logo'=>['type'=>'file'],
                ])

                <div class="raw-margin-top-24">
                    <a class="btn btn-default pull-left" href="{!! route('brands.index') !!}">Batal</a>
                    <button class="btn btn-primary pull-right" type="submit">Buat</button>
                </div>

            {!! Form::close() !!}
        </div> 

    </div>
</div>
@stop
