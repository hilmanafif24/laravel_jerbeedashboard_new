@extends('dashboard')

@section('content')

<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        {!! Form::open(['route' => 'brands.search','class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
        {!! csrf_field() !!}
        <input class="form-control form-inline pull-right" name="search" placeholder="Search">
        {!! Form::close() !!}
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('brands.create') !!}">Tambah Baru</a>
        <h1 class="pull-left"><span class="fa fa-flag-o"></span> Kelola Merek</h1>
    </div>
</div>
<div class="row raw-margin-top-24">
    <div class="col-md-12">
        @if($brands->isEmpty())
        <div class="well text-center">Merek Tidak Ditemukan.</div>
        @else
        <table class="table table-striped">
            <thead>
                <th>Nama</th>
                <th width="140px" class="text-right">Aksi</th>
            </thead>
            <tbody>
                @foreach($brands as $brand)
                <tr>
                    <td>
                        {{ $brand->name }}
                    </td>
                    <td>
                        <form method="post" action="{!! route('brands.destroy', [$brand->id]) !!}">
                            {!! csrf_field() !!}
                            {!! method_field('DELETE') !!}
                            <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this brand?')"><i class="fa fa-trash"></i> Hapus</button>
                        </form>
                        <!-- {{-- <a class="btn btn-warning btn-xs" href="{!! route('brands.show', [$brand->id]) !!}"><i class="fa fa-search"></i> Show</a> --}} -->
                        <a class="btn btn-warning btn-xs" href="{!! route('brands.edit', [$brand->id]) !!}"><i class="fa fa-pencil"></i> Ubah</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        <div class="row text-center">
            {!! $brands; !!}
        </div>
        @endif
    </div>
</div>

@stop
