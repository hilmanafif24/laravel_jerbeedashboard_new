<!DOCTYPE html> 
<html>
<head>
  <title>PDAM Tirta Raharja</title>

  <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="/css/style_slide.css" />

  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://example.com/slide-fade-content/jquery.colorfade.min.js"></script>

<script type="text/javascript">
// slide & fade content @ https://m0n.co/r
$(document).ready(function() {
  $('.more').on('click', function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    if ($('#ajax').is(':visible')) {
      $('#ajax').css({ display:'block' }).animate({ height:'0' }).empty();
    }
    $('#ajax').css({ display:'block' }).animate({ height:'200px' },function() {
      $('#ajax').html('<img id="loader" src="http://example.com/loader.gif">');
      $('#loader').css({ border:'none', position:'relative', top:'24px', left:'48px', boxShadow:'none' });
      $('#ajax').load('http://example.com/slide-fade-content.html ' + href, function() {
        $('#ajax').hide().fadeIn('slow').colorFade({ 'fadeColor': 'rgb(253,253,175)' });
        });
      });
    });
  });
  </script>

  @section('javascript')
@if($dmaselect->id != null)
<script src="{{ url('js/highcharts.js') }}"></script>
<script src="{{ url('js/themes/sand-signika.js') }}"></script>
<script src="{{ url('js/plugins/jquery.cookie.js') }}"></script>
<script src="{{ url('js/plugins/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugins/jquery.uniform.min.js') }}"></script>

<script type="text/javascript">
$(function () {

 $('#pilarmas').highcharts({
        chart: {
            zoomType: 'x'
        },
        title: {
            text: '{{ $dmaselect->name }}'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                    'Klik dan geser untuk memperbesar' : ''
        },
            xAxis: {
      gridLineWidth: 1,
            type: 'datetime',
        tickInterval: 3600 * 1000,
      labels: {
        style: {
        color: '#000000'
        }
      },
      crosshair: true
        },
        yAxis: [{
      // Primary yAxis
      gridLineWidth: 1,
      labels: {
                format: '{value}',
                style: {
                    color: '#999999'
                }
            },
            title: {
            text: 'Flow (m³)',
      style: {
                    color: '#999999'
                }
        },
      min: 0
        },
    {
      // Primary yAxis
      gridLineWidth: 1,
      labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[8]
                }
            },
            title: {
            text: 'Pressure (Bar)',
      style: {
                    color: Highcharts.getOptions().colors[8]
                }
        },
    opposite: true,
      min: 0
        }],
        tooltip: {
        shared: true,
    xDateFormat: '%A, %d-%m-%Y %H:%M'
        },

    legend: {
      enabled: true,
            layout: 'vertical',
            align: 'left',
            x: 57,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },

        series: [{
      cropThreshold: 720,
            type: 'spline',
      color: '#3fabd4',
            name: 'Flow',
      yAxis: 0,
            data: [{{ $flows }}],
      //marker: { enabled: true, radius: 2.5 },
      lineWidth: 1.5,
      tooltip: {
                valueSuffix: ' L/s'
            }
        },
    {
      cropThreshold: 720,
            type: 'spline',
      color: '#ff0099',
            name: 'Pressure',
      yAxis: 1,
            data: [{{ $pressures }}],
      //marker: { enabled: true, radius: 2.5 },
      lineWidth: 1.5,
      tooltip: { valueSuffix: ' Bar' }
      }]
    });

  });

</script>
@endif
@endsection

</head>
<body>

    <!-- Full Page Image Background Carousel Header -->
  <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <div class="row">
                  <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
                  <h1 class="pull-left"><span class="fa fa-area-chart"></span> Chart</h1>
                  </div>
                </div>

                <div class="row raw-margin-top-24">
                  <div class="col-md-6">
                    <p style="color:#999; font-size:12px;">Sample data valid gunakan DMA Pilarmas. Data real lebih lengkap sedang dikembangkan.</p>
                    <br />
                  </div>
    </div>

    <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body" style="background:#eee;">
              <div class="col-md-8">
                <div class="row">
                  {!! Form::open(['route' => 'nrws.chartmeasurement','method' => 'get']) !!}
                  {!! csrf_field() !!}
                 
                  <div class="col-md-3">
                  <select class="form-control" name="dma_id" id="dma_id">
                    <option value="">Tanpa DMA</option>
                    @foreach ($dmas as $dma)
                      @if($dmaselect->id == $dma->id)
                        <option value="{{$dma->id}}" selected>{{ $dma->name }}</option>
                      @else
                        <option value="{{$dma->id}}">{{ $dma->name }}</option>
                      @endif
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-1">
                  <button class="btn btn-primary">Generate Chart</button>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>
            </div>

            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/conference.jpg');"></div>
                <div class="carousel-caption">
                     <h2 class="animated fadeInDown">DMA A</h2>
                     <!-- <p class="animated fadeInUp">Lorem ipsum dolor sit amet consectetur adipisicing elit</p> -->
                     <p class="animated fadeInUp"><a href="#" class="btn btn-transparent btn-rounded btn-large">Selengkapnya</a></p>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/campus.jpg');"></div>
                <div class="carousel-caption">
                     <h2 class="animated fadeInRight">DMA B</h2>
                     <!-- <p class="animated fadeInRight">Lorem ipsum dolor sit amet consectetur adipisicing elit</p> -->
                     <p class="animated fadeInRight"><a href="#" class="btn btn-transparent btn-rounded btn-large">Selengkapnya</a></p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </div>
</body>

  <script type="text/javascript" src="/js/bootstrap.min.js"></script>

</html>