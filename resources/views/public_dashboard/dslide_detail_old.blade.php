<!DOCTYPE html>
<html>
<head>
  <title>PDAM Tirta Raharja</title>

  <link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
  <link rel="stylesheet" type="text/css" href="/css/style_slide.css" />

  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script type="text/javascript" src="http://example.com/slide-fade-content/jquery.colorfade.min.js"></script>

<script type="text/javascript">
// slide & fade content @ https://m0n.co/r
$(document).ready(function() {
  $('.more').on('click', function(e) {
    e.preventDefault();
    var href = $(this).attr('href');
    if ($('#ajax').is(':visible')) {
      $('#ajax').css({ display:'block' }).animate({ height:'0' }).empty();
    }
    $('#ajax').css({ display:'block' }).animate({ height:'200px' },function() {
      $('#ajax').html('<img id="loader" src="http://example.com/loader.gif">');
      $('#loader').css({ border:'none', position:'relative', top:'24px', left:'48px', boxShadow:'none' });
      $('#ajax').load('http://example.com/slide-fade-content.html ' + href, function() {
        $('#ajax').hide().fadeIn('slow').colorFade({ 'fadeColor': 'rgb(253,253,175)' });
        });
      });
    });
  });
  </script>

</head>
<body>

    <!-- Full Page Image Background Carousel Header -->
  <div id="myCarousel" class="carousel slide">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
            <div class="item active">
                <!-- Set the first background image using inline CSS below. -->
                <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/code.jpg');"></div>
                <div class="carousel-caption">
                     <h2 class="animated fadeInLeft">Caption Animation</h2>
                     <p class="animated fadeInUp">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                     <!-- 
                     <p class="animated fadeInUp"><a href="#" class="btn btn-transparent btn-rounded btn-large">Learn More</a></p> -->
                </div>
            </div>
            <div class="item">
                <!-- Set the second background image using inline CSS below. -->
                <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/conference.jpg');"></div>
                <div class="carousel-caption">
                     <h2 class="animated fadeInDown">Caption Animation</h2>
                     <p class="animated fadeInUp">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                     <p class="animated fadeInUp"><a href="#" class="btn btn-transparent btn-rounded btn-large">Learn More</a></p>
                </div>
            </div>
            <div class="item">
                <!-- Set the third background image using inline CSS below. -->
                <div class="fill" style="background-image:url('http://www.marchettidesign.net/demo/optimized-bootstrap/campus.jpg');"></div>
                <div class="carousel-caption">
                     <h2 class="animated fadeInRight">Caption Animation</h2>
                     <p class="animated fadeInRight">Lorem ipsum dolor sit amet consectetur adipisicing elit</p>
                     <p class="animated fadeInRight"><a href="#" class="btn btn-transparent btn-rounded btn-large">Learn More</a></p>
                </div>
            </div>
        </div>

        <!-- Controls -->
        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#myCarousel" data-slide="next">
            <span class="icon-next"></span>
        </a>

    </div>


</body>

  <script type="text/javascript" src="/js/bootstrap.min.js"></script>

</html>