@extends('dashboard')

@section('content')

<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-map-marker"></span> Ubah Posisi</h1>
    </div>
</div>
<br>
  <div class="row">
      <div class="col-md-6 raw-margin-bottom-24">
          <div>
              {!! Form::model($location, ['route' => ['locations.update', $location->id], 'method' => 'patch']) !!}
                  {!! csrf_field() !!}
                  {!! method_field('PATCH') !!}

                  @form_maker_object($location, FormMaker::getTableColumns('locations'))

                  <div class="raw-margin-top-24">
                      <a class="btn btn-default pull-left" href="{!! route('locations.index') !!}">Batal</a>
                      <button class="btn btn-primary pull-right" type="submit">Simpan</button>
                  </div>

            {!! Form::close() !!}
          </div>
      </div>
  </div>

@stop
