@extends('dashboard')

@section('pre-javascript')
<script src="{{ url('js/d3.js') }}"></script>
@stop

@section('content')

    <style>
    @media (max-width:800px) {
      .wrapper1 { display:none; }
      .wrapper, .wrapper2, .wrapper3 { width:160px!important; }
      .wrapperbig { width:323px!important; }
    }
    </style>

    <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
            <h1 class="pull-left"><span class="fa fa-tachometer"></span> Water Balance</h1>
        </div>
    </div>

    <div class="row raw-margin-top-24">
        <div class="col-md-6">
          <p style="color:#999; font-size:12px;">Sample data valid gunakan DMA Pilarmas. Data real lebih lengkap sedang dikembangkan.</p>
          <br />
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body" style="background:#eee;">
              <div class="col-md-8">
                <div class="row">
                  {!! Form::open(['route' => 'nrws.waterbalance','method' => 'get']) !!}
                  {!! csrf_field() !!}
                  <div class="col-md-3">
                  <select class="form-control" name="spam_id" id="spam_id">
                    <option value="">Tanpa SPAM</option>
                    @foreach ($spams as $spam)
                      @if($spamselect->id == $spam->id)
                        <option value="{{$spam->id}}" selected>{{ $spam->name }}</option>
                      @else
                        <option value="{{$spam->id}}">{{ $spam->name }}</option>
                      @endif
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-3">
                  <select class="form-control" name="zona_id" id="zona_id">
                    <option value="">Tanpa Zona</option>
                    @foreach ($zonas as $zona)
                      @if($zonaselect->id == $zona->id)
                        <option value="{{$zona->id}}" selected>{{ $zona->name }}</option>
                      @else
                        <option value="{{$zona->id}}">{{ $zona->name }}</option>
                      @endif
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-3">
                  <select class="form-control" name="dma_id" id="dma_id">
                    <option value="">Tanpa DMA</option>
                    @foreach ($dmas as $dma)
                      @if($dmaselect->id == $dma->id)
                        <option value="{{$dma->id}}" selected>{{ $dma->name }}</option>
                      @else
                        <option value="{{$dma->id}}">{{ $dma->name }}</option>
                      @endif
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-1">
                  <button class="btn btn-primary">Generate Chart</button>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    @if($dmaselect->id != null)
    <div class="row">
        <div class="col-md-12">
        <div class="wrapperbig" style="width:603px;">
          <div id="wrapper" class="wrapper" style="float:left; height: 496px; width: 200px; position: relative; margin-right:1px;">
          </div>

          <div id="wrapper1" class="wrapper1" style="float:left; height: 496px; width: 200px; position: relative; margin-right:1px;">
          </div>

          <div id="wrapper2" class="wrapper2" style="float:right; height: 400px; width: 200px; position: relative; margin-right:1px;">
          </div>

          <div id="wrapper3" class="wrapper3" style="float:right; height: 96px; width: 200px; position: relative; margin-right:1px;">
          </div>
        </div>
        </div>
    </div>
    @endif

    <br /><br />
@stop

@section('javascript')
<script type="text/javascript">
$(document).ready(function () {
var log = console.log.bind(console);
console.clear();

(function ($) {
  $.fn.analogTank = function (config) {
    let $this = $(this);

    return new AnalogTank($this, config);
  };

  class AnalogTank {
    constructor($this, config) {
      let defaults = {
        tankType: "tower", // available types: 'tower', 'round'
        tankWidth: null, // outside width.
        tankHeight: null, // outside height.
        fillPadding: null, // gap between perimeter and inside tank area that displays water.
        borderWidth: 0, // perimeter width.
        borderColor: "#ddd", // outside border color. usually the perimeter of the tank
        defaultFillColor: "#3fabd4", // default water color. this is assigned to fillColor if water level does not pass any thresholds.
        fillColor: null, // used later to set water color. it could be different color depending on situations.
        backFillColor: "#f0f0f0", // background color inside the tank where there is no water.
        backFillOpacity: 1, // opacity of the background.
        innerCornerRadius: 0,
        borderCornerRadius: 0,
        innerWidth: null,
        innerHeight: null,
        fillAnimationColor: null, // used later to set the color while animating.
        fillMaxValue: 100, // maximum possible value for the main text.
        fillMinValue: 0, // minimum possible value for the main text.
        fillValue: null, // value used to display the main text.
        fillUnit: null, // unit that is appended to the main text.
        decimal: 1, // number of decimal places for the main text.
        overlayTextFillOpacity: 0.8, // opacity of the main text.
        fontFamily: "Helvetica",
        fontWeight: "bold",
        fontSize: 20,
        backFontColor: null,
        backFontAnimationColor: null,
        frontFontColor: null,
        waveWidth: 70,
        amplitude: 7,
        horizontalWaveDuration: 2000,
        transitionDuration: 1000,
        delay: 0,
        ease: d3.easePolyInOut.exponent(4),
        tooltipFontSize: 10,
        thresholds: [],
        lookupTableValue: null, // if lookup table value is set, a secondary text is displayed under the main text.
        lookupTableValueUnit: null, // unit for lookupTableValue.
        lookupTableValueDecimal: 0, // number of decimal places for lookup table value.
        lookupTableValueEnabled: false,
        lookupTableValueFontSize: 14,
        lookupTableValueYOffset: 2,
        changeRateValue: null,
        changeRateValueDecimal: 0,
        changeRateValueEnabled: false,
        changeRateValueFontSize: 14,
        changeRateValueYOffset: 2,
        changeRateValueUnit: 'gal/min'
      };
      Object.assign(defaults, config);

      this.container = $this;
      Object.assign(this, defaults);
      this.url = window.location.href;

      this.init();
    }

    // initializer
    init() {
      this.setInitialValues();
      this.drawSvgContainer();

      if ( this.tankType === "tower" ) {
        this.initTower();
      } else if ( this.tankType === "round" ) {
        this.initRound();
      }

      this.calculateDimensions();
      this.setGaugeScale();
      this.getNewHeight();
      this.applyFillAttributes();
      this.applyTextAttributes();
      this.applyWaveHorizontalAttributes();
      this.tweenWaveHorizontal();
      this.animateFromZero();
    }

    drawSvgContainer() {
      this.width = this.container.outerWidth();
      this.height = this.container.outerHeight();
      let viewBoxDef = `0, 0, ${this.width}, ${this.height}`;

      this.svgContainer = d3.select(this.container[0])
        .attr('id', 'svg-container')
        .append("svg")
        .attr("width", "100%")
        .attr("height", "100%")
        .attr("viewBox", viewBoxDef);

      this.bodyGroup = this.svgContainer.append('g')
        .attr('id', 'body-group')
        .attr('transform', `translate(${this.width/2}, ${this.height/2})`);
    }

    // scale that returns pixel value for positioning the waveClip vertically
    setGaugeScale () {
      this.gaugeScale = d3.scaleLinear()
        .domain([this.fillMinValue, this.fillMaxValue])
        .range([(this.innerHeight + this.amplitude)/2, -(this.innerHeight + this.amplitude)/2])
        .clamp(true);
    }

    getNewHeight () {
      this.newHeight = this.fillValue === null ? 0 : this.gaugeScale(this.fillValue);
    }

    initTower () {
      let uniqId = this.uniqId();
      // this.tankGroup = this.bodyGroup.append('g').attr('id', 'tank-group');
      this.waveClip = this.bodyGroup.append('defs').append('clipPath').attr('id', uniqId);
      this.waveHorizontal = this.waveClip.append('path');

      this.backFill = this.bodyGroup.append('rect').attr('id', 'back-fill');
      this.border = this.bodyGroup.append('rect').attr('id', 'border');
      this.behindText = this.bodyGroup.append('text').attr('id', 'behind-text');

      if (this.lookupTableValueEnabled) {
        this.lookupTableValueBehindText = this.bodyGroup.append('text').attr('id', 'lookup-value-behind-text');
      }

      if (this.changeRateValueEnabled) {
        this.changeRateValueBehindText = this.bodyGroup.append('text').attr('id', 'change-rate-value-behind-text');
      }

      this.waveGroup = this.bodyGroup.append('g').attr('clip-path', this.getUniqUrl(uniqId));
      this.waterFill = this.waveGroup.append('rect').attr('id', 'water-fill');

      this.overlayText = this.waveGroup.append('text').attr('id', 'overlay-text');

      if (this.lookupTableValueEnabled) {
        this.lookupTableValueOverlayText = this.waveGroup.append('text').attr('id', 'lookup-value-overlay-text');
      }

      if (this.changeRateValueEnabled) {
        this.changeRateValueOverlayText = this.waveGroup.append('text').attr('id', 'change-rate-value-overlay-text');
      }

      // for debug purpose
      // this.bodyGroup.append('path').attr('d', 'M-${this.height/2} 0 L ${this.height} 0').attr('stroke-width', 1).attr('stroke', 'red');
    }

    initRound () {
      let that = this;
      let uniqId = this.uniqId();
      this.tankGroup = this.bodyGroup.append('g').attr('id', 'tank-group');
      this.waveClip = this.bodyGroup.append('defs').append('clipPath').attr('id', uniqId);
      this.waveHorizontal = this.waveClip.append('path');

      this.behindText = this.tankGroup.append('text').attr('id', 'behind-text');

      if (this.lookupTableValueEnabled) {
        this.lookupTableValueBehindText = this.tankGroup.append('text').attr('id', 'lookup-value-behind-text');
      }

      if (this.changeRateValueEnabled) {
        this.changeRateValueBehindText = this.tankGroup.append('text').attr('id', 'lookup-value-rate-behind-text');
      }

      this.waveGroup = this.tankGroup.append('g').attr('clip-path', this.getUniqUrl(uniqId));

      this.overlayText = this.waveGroup.append('text').attr('id', 'overlay-text');

      if (this.lookupTableValueEnabled) {
        this.lookupTableValueOverlayText = this.waveGroup.append('text').attr('id', 'lookup-value-overlay-text');
      }

      if (this.changeRateValueEnabled) {
        this.changeRateValueOverlayText = this.waveGroup.append('text').attr('id', 'lookup-value-rate-overlay-text');
      }

    }

    // sets the inital text and color to display based on fillValue
    setInitialValues () {
      this.lookupTableValueEnabled = this.lookupTableValue !== null ? true : false;
      this.changeRateValueEnabled = this.changeRateValue !== null ? true : false;

      this.fillColor = this.defaultFillColor;
    }

    calculateDimensions() {

      if (this.tankType === 'tower') {
        this.tankWidth = this.width - this.borderWidth;
        this.tankHeight = this.height - this.borderWidth;

        if ( this.fillPadding !== null && typeof this.fillPadding === "number" && this.fillPadding !== 0 ) {
          this.innerWidth = this.tankWidth - 2*this.fillPadding; // in case there is a padding, this will be the inner fill part
          this.innerHeight = this.tankHeight - 2*this.fillPadding; // same as above
        } else {
          this.innerWidth = this.tankWidth - this.borderWidth;
          this.innerHeight = this.tankHeight - this.borderWidth;
        }

      }
    }

    applyFillAttributes() {
      if (this.tankType === 'tower') {
        this.applyAttributes(this.backFill, {
          x: 0,
          y: 0,
          width: this.innerWidth,
          height: this.innerHeight,
          rx: this.innerCornerRadius,
          ry: this.innerCornerRadius,
          fill: this.backFillColor,
          'fill-opacity': this.backFillOpacity,
          transform: `translate(-${(this.tankWidth - this.borderWidth)/2}, -${(this.tankHeight - this.borderWidth)/2})`
        });

        this.applyAttributes(this.waterFill, {
          datum: { color: this.fillColor },
          x: 0,
          y: 0,
          width: this.innerWidth,
          height: this.innerHeight,
          rx: this.innerCornerRadius,
          ry: this.innerCornerRadius,
          fill: function(d) { return d.color; },
          transform: `translate(-${(this.tankWidth - this.borderWidth)/2}, -${(this.tankHeight - this.borderWidth)/2})`
        });

        this.applyAttributes(this.border, {
          x: 0,
          y: 0,
          width: this.tankWidth,
          height: this.tankHeight,
          rx: this.borderCornerRadius,
          ry: this.borderCornerRadius,
          'fill-opacity': 0,
          stroke: this.borderColor,
          'stroke-width': this.borderWidth,
          transform: `translate(-${this.tankWidth/2}, -${this.tankHeight/2})`
        });
      } else if (this.tankType === 'round') {
        this.applyAttributes(this.tankGroup, {
          transform: `translate(0, -${this.height/2 - this.tankRy - this.borderWidth/2})`
        });

        this.applyAttributes(this.backFill, {
          cx: 0,
          cy: 0,
          rx: this.innerRx,
          ry: this.innerRy,
          fill: this.backFillColor
        });

        this.applyAttributes(this.waterFill, {
          datum: { color: this.fillColor },
          cx: 0,
          cy: 0,
          rx: this.innerRx,
          ry: this.innerRy,
          fill: this.fillColor
        });

        this.applyAttributes(this.border, {
          cx: 0,
          cy: 0,
          rx: this.tankRx,
          ry: this.tankRy,
          'fill-opacity': 0,
          stroke: this.borderColor,
          'stroke-width': this.borderWidth
        });

        let topRight = `${xCoord} ${this.tankRy/8*7}`;
        let bottomRight = `${this.tankRx/4} ${this.height - this.tankRy - this.borderWidth/2}`;

        let topLeft = `-${xCoord} ${this.tankRy/8*7}`;
        let bottomLeft = `-${this.tankRx/4} ${this.height - this.tankRy - this.borderWidth/2}`;

        let topRightInflectionPt = `${this.tankRx/4} ${this.tankRy}`;
        let bottomRightInflectionPt = `${this.tankRx/4} ${this.tankRy}`;
        let topLeftInflectionPt = `-${this.tankRx/4} ${this.tankRy}`;
        let bottomLeftInflectionPt = `-${this.tankRx/4} ${this.tankRy}`;

        // for debug purpose
        // let topRightInfxPt = this.tankGroup.append('circle').attr('r', 2).attr('fill','red').attr('transform', `translate(${topRightInflectionPt})`);
        // let bottomRightInfxPt = this.tankGroup.append('circle').attr('r', 2).attr('fill','red').attr('transform', `translate(${bottomRightInflectionPt})`);

      }
    }

    applyTextAttributes() {
      let transform = `translate(0, ${this.fontSize/4})`;

      this.applyAttributes(this.behindText, {
        datum: { color: this.backFontColor === null ? this.fillColor : this.backFontColor },
        'text-anchor': 'middle',
        'font-family': this.fontFamily,
        'font-size': `${this.fontSize}px`,
        'font-weight': this.fontWeight,
        fill: function(d) { return d.color; },
        text: `0 ${this.fillUnit}`,
        transform: transform
      });

      this.applyAttributes(this.overlayText, {
        datum: { color: this.frontFontColor === null ? "#fff" : this.frontFontColor },
        'text-anchor': 'middle',
        'font-family': this.fontFamily,
        'font-size': `${this.fontSize}px`,
        'font-weight': this.fontWeight,
        fill: function(d) { return d.color; },
        'fill-opacity': this.overlayTextFillOpacity,
        text: `0 ${this.fillUnit}`,
        transform: transform
      });

      if (this.lookupTableValueEnabled) {
        let lookupTransform = `translate(0, ${this.fontSize/4 + this.lookupTableValueFontSize + this.lookupTableValueYOffset})`;

        this.applyAttributes(this.lookupTableValueBehindText, {
          datum: { color: this.backFontColor === null ? this.fillColor : this.backFontColor },
          'text-anchor': 'middle',
          'font-family': this.fontFamily,
          'font-size': `${this.lookupTableValueFontSize}px`,
          'font-weight': this.fontWeight,
          fill: function(d) { return d.color; },
          text: `0 ${this.lookupTableValueUnit}`,
          transform: lookupTransform
        });

        this.applyAttributes(this.lookupTableValueOverlayText, {
          datum: { color: this.frontFontColor === null ? "#fff" : this.frontFontColor },
          'text-anchor': 'middle',
          'font-family': this.fontFamily,
          'font-size': `${this.lookupTableValueFontSize}px`,
          'font-weight': this.fontWeight,
          fill: function(d) { return d.color; },
          'fill-opacity': this.overlayTextFillOpacity,
          text: `0 ${this.lookupTableValueUnit}`,
          transform: lookupTransform
        });
      }

      if (this.changeRateValueEnabled) {
        let yOffset = this.fontSize/4 + this.changeRateValueFontSize + this.changeRateValueYOffset;

        if (this.lookupTableValueEnabled) {
          yOffset += this.lookupTableValueFontSize + this.lookupTableValueYOffset;
        }

        let rateTransform = `translate(0, ${yOffset})`;

        this.applyAttributes(this.changeRateValueBehindText, {
          datum: { color: this.backFontColor === null ? this.fillColor : this.backFontColor },
          'text-anchor': 'middle',
          'font-family': this.fontFamily,
          'font-size': `${this.changeRateValueFontSize}px`,
          'font-weight': this.fontWeight,
          fill: function(d) { return d.color; },
          text: `0 ${this.changeRateValueUnit}`,
          transform: rateTransform
        });

        this.applyAttributes(this.changeRateValueOverlayText, {
          datum: { color: this.frontFontColor === null ? "#fff" : this.frontFontColor },
          'text-anchor': 'middle',
          'font-family': this.fontFamily,
          'font-size': `${this.changeRateValueFontSize}px`,
          'font-weight': this.fontWeight,
          fill: function(d) { return d.color; },
          'fill-opacity': this.overlayTextFillOpacity,
          text: `0 ${this.changeRateValueUnit}`,
          transform: rateTransform
        });
      }
    }

    applyWaveHorizontalAttributes() {
      this.clipDef = `M0 0 Q${this.waveWidth/2} ${this.amplitude}, ${this.waveWidth} 0 T${2*this.waveWidth} 0`;
      var minRequiredClipWidth = this.width*2 + 2*this.waveWidth + this.borderWidth/2;
      this.clipWidth = 2*this.waveWidth;

      while ( this.clipWidth < minRequiredClipWidth ) {
        this.clipWidth += this.waveWidth;
        this.clipDef += ` T${this.clipWidth} 0`;
        this.clipWidth += this.waveWidth;
        this.clipDef += ` T${this.clipWidth} 0`;
      }
      this.clipDefArray = [this.clipDef, `L${this.clipWidth}`, `${this.height}`, "L0", `${this.height}`, "Z"];
      this.clipDef = this.clipDefArray.join(" ");

      this.applyAttributes(this.waveHorizontal, {
        d: this.clipDef
      });
    }


    tweenWaveHorizontal () {
      let that = this;
      let startHeight = (this.tankHeight - this.innerHeight)/2 - this.amplitude/2;
      let transformStart = `translate(-${this.width + 2*this.waveWidth}, ${startHeight})`;
      let transformEnd = `translate(-${this.width}, ${startHeight})`;

      this.waveHorizontal.attr('transform', transformStart);

      animate();

      function animate() {
        that.waveHorizontal
          .transition()
          .duration(that.horizontalWaveDuration)
          .ease(d3.easeLinear)
          .attrTween("transform", function(d) {
            return d3.interpolateString(transformStart, transformEnd);
          }).on("end", function (d) {
          animate();
        });
      }
    }

    animateFromZero () {
      this.waveClip
        .datum({ transform: `translate(0, ${this.gaugeScale(this.fillMinValue) + this.amplitude})`})
        .attr('transform', function(d) { return d.transform; });
      this.animateNewHeight(this.fillValue);
    }

    animateNewHeight (val) {
      let that = this;
      if ( typeof val !== "undefined" ) {
        this.newHeight = this.gaugeScale(val);
        this.fillValue = val;
      }

      this.tweenWaveVertical();
      this.tweenElements();
    }

    tweenWaveVertical() {
      let endTransform = `translate(0, ${this.newHeight})`;

      return this.waveClip
        .transition()
        .delay(this.delay)
        .duration(this.transitionDuration)
        .ease(this.ease)
        .attrTween("transform", function(d) {
          let interpolator = d3.interpolateString(d.transform, endTransform);

          return function(t) {
            d.transform = interpolator(t);
            return d.transform;
          };
        });
    }

    calculateColor () {
      this.fillAnimationColor = this.fillColor === null ? this.defaultFillColor : this.fillColor;
      this.backFontAnimationColor = this.backFontColor === null ? this.fillColor : this.backFontColor;
    }

    tweenElements () {
      this.calculateColor();
      this.colorTransition(this.waterFill, "fill", this.fillAnimationColor);
      this.tweenText();
    }

    colorTransition(selection, attribute, targetColor) {
      selection
        .transition()
        .delay(this.delay)
        .duration(this.transitionDuration)
        .ease(this.ease)
        .attrTween(attribute, function(d) {
          let interpolator = d3.interpolateRgb(d.color, targetColor);

          return function(t) {
            d.color = interpolator(t);
            return d.color;
          };
        });
    }

    textFormatter(val) {
      if (this.fillUnit) {
        return `${(Number(Math.round(parseFloat(val) + 'e' + this.decimal) + 'e-' + this.decimal)).toFixed(this.decimal)} ${this.fillUnit}`;
      }
      return `${(Number(Math.round(parseFloat(val) + 'e' + this.decimal) + 'e-' + this.decimal)).toFixed(this.decimal)}`;
    }

    lookupTextFormatter(val) {
      if (this.lookupTableValueUnit) {
        return `${Number(Math.round(parseFloat(val) + 'e' + this.lookupTableValueDecimal) + 'e-' + this.lookupTableValueDecimal).toFixed(this.lookupTableValueDecimal)} ${this.lookupTableValueUnit}`;
      }
      return `${Number(Math.round(parseFloat(val) + 'e' + this.lookupTableValueDecimal) + 'e-' + this.lookupTableValueDecimal).toFixed(this.lookupTableValueDecimal)}`;
    }

    changeRateValueTextFormatter(val) {
      // ngilangin integer/counternya, jadi bari teks doang
      //if (this.changeRateValueUnit) {
      //  return `${Number(Math.round(parseFloat(val) + 'e' + this.changeRateValueDecimal) + 'e-' + this.changeRateValueDecimal).toFixed(this.changeRateValueDecimal)} ${this.changeRateValueUnit}`;
      //}
      //return `${Number(Math.round(parseFloat(val) + 'e' + this.changeRateValueDecimal) + 'e-' + this.changeRateValueDecimal).toFixed(this.changeRateValueDecimal)}`;
      return `${this.changeRateValueUnit}`;
    }

    tweenText() {
      let that = this;

      this.behindText
        .transition()
        .delay(this.delay)
        .ease(this.ease)
        .duration(this.transitionDuration)
        .tween("text", function(d) {
          let node = this;
          let interpolate = d3.interpolate(that.textFormatter(node.textContent), that.textFormatter(that.fillValue));

          return function(t) {
            node.textContent = that.textFormatter(interpolate(t));
          };
        })
        .attrTween("fill", function(d) {
          let interpolator = d3.interpolateRgb(d.color, that.backFontAnimationColor);

          return function(t) {
            d.color = interpolator(t);
            return d.color;
          };
        });

      this.overlayText
        .transition()
        .delay(this.delay)
        .ease(this.ease)
        .duration(this.transitionDuration)
        .tween("text", function(d) {
          let node = this;
          let interpolate = d3.interpolate(that.textFormatter(node.textContent), that.textFormatter(that.fillValue));
          return function(t) {
            node.textContent = that.textFormatter(interpolate(t));
          };
        })
        .attrTween("fill", function(d) {
          let interpolator = d3.interpolateRgb(d.color, that.frontFontColor);

          return function(t) {
            d.color = interpolator(t);
            return d.color;
          };
        })
        .on('end', function() {
        });

      if (this.lookupTableValueEnabled) {
        this.lookupTableValueBehindText
          .transition()
          .delay(this.delay)
          .ease(this.ease)
          .duration(this.transitionDuration)
          .tween("text", function(d) {
            let node = this;
            let interpolate = d3.interpolate(that.lookupTextFormatter(node.textContent), that.lookupTextFormatter(that.lookupTableValue));

            return function(t) {
              node.textContent = that.lookupTextFormatter(interpolate(t));
            };
          })
          .attrTween("fill", function(d) {
            let interpolator = d3.interpolateRgb(d.color, that.backFontAnimationColor);

            return function(t) {
              d.color = interpolator(t);
              return d.color;
            };
          });

        this.lookupTableValueOverlayText
          .transition()
          .delay(this.delay)
          .ease(this.ease)
          .duration(this.transitionDuration)
          .tween("text", function(d) {
            let node = this;
            let interpolate = d3.interpolate(that.lookupTextFormatter(node.textContent), that.lookupTextFormatter(that.lookupTableValue));
            return function(t) {
              node.textContent = that.lookupTextFormatter(interpolate(t));
            };
          })
          .attrTween("fill", function(d) {
            let interpolator = d3.interpolateRgb(d.color, that.frontFontColor);

            return function(t) {
              d.color = interpolator(t);
              return d.color;
            };
          });
      }

      if (this.changeRateValueEnabled) {
        this.changeRateValueBehindText
          .transition()
          .delay(this.delay)
          .ease(this.ease)
          .duration(this.transitionDuration)
          .tween("text", function(d) {
            let node = this;
            let interpolate = d3.interpolate(that.changeRateValueTextFormatter(node.textContent), that.changeRateValueTextFormatter(that.changeRateValue));

            return function(t) {
              node.textContent = that.changeRateValueTextFormatter(interpolate(t));
            };
          })
          .attrTween("fill", function(d) {
            let interpolator = d3.interpolateRgb(d.color, that.backFontAnimationColor);

            return function(t) {
              d.color = interpolator(t);
              return d.color;
            };
          });

        this.changeRateValueOverlayText
          .transition()
          .delay(this.delay)
          .ease(this.ease)
          .duration(this.transitionDuration)
          .tween("text", function(d) {
            let node = this;
            let interpolate = d3.interpolate(that.changeRateValueTextFormatter(node.textContent), that.changeRateValueTextFormatter(that.changeRateValue));
            return function(t) {
              node.textContent = that.changeRateValueTextFormatter(interpolate(t));
            };
          })
          .attrTween("fill", function(d) {
            let interpolator = d3.interpolateRgb(d.color, that.frontFontColor);

            return function(t) {
              d.color = interpolator(t);
              return d.color;
            };
          });
      }
    }

    applyAttributes(selection, datum = {}) {
      let properties = Object.getOwnPropertyNames(datum);
      properties.forEach((p) => {
        if (p === 'datum') {
          return selection.datum( datum[p] );
        } else if (p === 'text') {
          return selection.text( datum[p] );
        } else if (p === 'style') {
          return selection.style( datum[p] );
        } else {
          return selection.attr( p, datum[p] );
        }
      });
    }

    getHeight(selection) {
      return selection.node().getBBox().height;
    }

    getMaxWidth (first, second) {
      if (typeof second === 'object') {
        return Math.max(first.node().getBBox().width, second.node().getBBox().width);
      }
      return first.node().getBBox().width;
    }


    // utility functions
    uniqId() {
      // Convert it to base 36 (numbers + letters), and grab the first 9 characters
      // after the decimal.
      return "clipPath" + Math.random().toString(36).substr(2, 9);
    }
    getUniqUrl(id) {
      return `url(${this.url}#${id})`;
    }

  } // end of class

})(jQuery);

let options = {
  tankType: 'tower',
  fillValue: {{ $totalkubikasi }},
  fillUnit: "m³",
  frontFontColor: "#003B42",
  lookupTableValue: 100,
  lookupTableValueUnit: '%',
  lookupTableValueDecimal: 1,
  changeRateValueDecimal: 0,
  changeRateValue: 0,
  changeRateValueUnit: 'Volume Produksi',
  fillMaxValue: {{ $totalkubikasi + 30 }},
}

let options1 = {
  tankType: 'tower',
  fillValue: 496000,
  fillUnit: "m³",
  fontSize: 0,
  frontFontColor: "#003B42",
  lookupTableValueFontSize: 0,
  lookupTableValue: 100,
  lookupTableValueUnit: '%',
  lookupTableValueDecimal: 1,
  changeRateValueDecimal: 0,
  changeRateValue: 0,
  changeRateValueUnit: '(Sedang dikembangkan)',
  fillMaxValue: 516000,
}

let options2 = {
  tankType: 'tower',
  fillValue: {{ $totalpemakaian }},
  fillUnit: "m³",
  frontFontColor: "#003B42",
  lookupTableValue: 85,
  lookupTableValueUnit: '%',
  lookupTableValueDecimal: 0,
  changeRateValueDecimal: 0,
  changeRateValue: 0,
  changeRateValueUnit: 'Air berekening',
  fillMaxValue: {{ $totalpemakaian + 30 }},
}

let options3 = {
  tankType: 'tower',
  fillValue: {{ $totalkubikasi - $totalpemakaian }},
  fillUnit: "m³",
  frontFontColor: "#003B42",
  lookupTableValue: 15,
  lookupTableValueUnit: '%',
  lookupTableValueDecimal: 1,
  changeRateValueDecimal: 0,
  changeRateValue: 0,
  changeRateValueUnit: 'Air tak berekening',
  fillMaxValue: {{ $totalkubikasi - $totalpemakaian + 30 }},
  defaultFillColor: "#ff0099",
  backFillColor: "#3fabd4",
}

let tank = $('.wrapper').analogTank(options);
let tank1 = $('.wrapper1').analogTank(options1);
let tank2 = $('.wrapper2').analogTank(options2);
let tank3 = $('.wrapper3').analogTank(options3);

function updateLookupTableValue() {
  tank.updateLookupTableValue(parseInt(Math.random()*1000));
  tank1.updateLookupTableValue(parseInt(Math.random()*1000));
  tank2.updateLookupTableValue(parseInt(Math.random()*1000));
  tank3.updateLookupTableValue(parseInt(Math.random()*1000));
}
});
</script>
@endsection
