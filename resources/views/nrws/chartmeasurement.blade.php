@extends('dashboard')

@section('javascript')
@if($dmaselect->id != null)
<script src="{{ url('js/highcharts.js') }}"></script>
<script src="{{ url('js/themes/sand-signika.js') }}"></script>
<script src="{{ url('js/plugins/jquery.cookie.js') }}"></script>
<script src="{{ url('js/plugins/jquery.dataTables.min.js') }}"></script>
<script src="{{ url('js/plugins/jquery.uniform.min.js') }}"></script>

<script type="text/javascript">
$(function () {

 $('#pilarmas').highcharts({
        chart: {
            zoomType: 'x'
        },
        title: {
            text: '{{ $dmaselect->name }}'
        },
        subtitle: {
            text: document.ontouchstart === undefined ?
                    'Klik dan geser untuk memperbesar' : ''
        },
        		xAxis: {
			gridLineWidth: 1,
            type: 'datetime',
		    tickInterval: 3600 * 1000,
			labels: {
				style: {
				color: '#000000'
				}
			},
			crosshair: true
        },
        yAxis: [{
			// Primary yAxis
			gridLineWidth: 1,
			labels: {
                format: '{value}',
                style: {
                    color: '#999999'
                }
            },
            title: {
            text: 'Flow (m³)',
			style: {
                    color: '#999999'
                }
        },
	    min: 0
        },
		{
			// Primary yAxis
			gridLineWidth: 1,
			labels: {
                format: '{value}',
                style: {
                    color: Highcharts.getOptions().colors[8]
                }
            },
            title: {
            text: 'Pressure (Bar)',
			style: {
                    color: Highcharts.getOptions().colors[8]
                }
        },
		opposite: true,
	    min: 0
        }],
        tooltip: {
        shared: true,
		xDateFormat: '%A, %d-%m-%Y %H:%M'
        },

		legend: {
			enabled: true,
            layout: 'vertical',
            align: 'left',
            x: 57,
            verticalAlign: 'top',
            y: 0,
            floating: true,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },

        series: [{
			cropThreshold: 720,
            type: 'spline',
			color: '#3fabd4',
            name: 'Flow',
			yAxis: 0,
            data: [{{ $flows }}],
			//marker: { enabled: true, radius: 2.5 },
			lineWidth: 1.5,
			tooltip: {
                valueSuffix: ' L/s'
            }
        },
		{
			cropThreshold: 720,
            type: 'spline',
			color: '#ff0099',
            name: 'Pressure',
			yAxis: 1,
            data: [{{ $pressures }}],
			//marker: { enabled: true, radius: 2.5 },
			lineWidth: 1.5,
			tooltip: { valueSuffix: ' Bar' }
      }]
    });

  });

</script>
@endif
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
            <h1 class="pull-left"><span class="fa fa-area-chart"></span> Chart</h1>
        </div>
    </div>

    <div class="row raw-margin-top-24">
        <div class="col-md-6">
          <p style="color:#999; font-size:12px;">Sample data valid gunakan DMA Pilarmas. Data real lebih lengkap sedang dikembangkan.</p>
          <br />
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
          <div class="panel panel-default">
            <div class="panel-body" style="background:#eee;">
              <div class="col-md-8">
                <div class="row">
                  {!! Form::open(['route' => 'nrws.chartmeasurement','method' => 'get']) !!}
                  {!! csrf_field() !!}
                  <div class="col-md-3">
                  <select class="form-control" name="spam_id" id="spam_id">
                    <option value="">Tanpa SPAM</option>
                    @foreach ($spams as $spam)
                      @if($spamselect->id == $spam->id)
                        <option value="{{$spam->id}}" selected>{{ $spam->name }}</option>
                      @else
                        <option value="{{$spam->id}}">{{ $spam->name }}</option>
                      @endif
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-3">
                  <select class="form-control" name="zona_id" id="zona_id">
                    <option value="">Tanpa Zona</option>
                    @foreach ($zonas as $zona)
                      @if($zonaselect->id == $zona->id)
                        <option value="{{$zona->id}}" selected>{{ $zona->name }}</option>
                      @else
                        <option value="{{$zona->id}}">{{ $zona->name }}</option>
                      @endif
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-3">
                  <select class="form-control" name="dma_id" id="dma_id">
                    <option value="">Tanpa DMA</option>
                    @foreach ($dmas as $dma)
                      @if($dmaselect->id == $dma->id)
                        <option value="{{$dma->id}}" selected>{{ $dma->name }}</option>
                      @else
                        <option value="{{$dma->id}}">{{ $dma->name }}</option>
                      @endif
                    @endforeach
                  </select>
                  </div>
                  <div class="col-md-1">
                  <button class="btn btn-primary">Generate Chart</button>
                  </div>
                  {!! Form::close() !!}
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    @if($dmaselect->id != null)
    <div id="exTab2" class="container">
    <ul class="nav nav-tabs">
    			<li class="active"><a href="#1" data-toggle="tab">Chart</a></li>
    			<li><a href="#2" data-toggle="tab">Data Bulanan (per-jam)</a></li>
    			<li><a href="#3" data-toggle="tab">Data Bulanan (per-hari)</a></li>
    </ul>
    <div class="tab-content ">
      <div class="tab-pane active" id="1">
        <div id="chart" class="subcontent" >
        <div id="pilarmas"></div>
        </div>
      </div>
      <div class="tab-pane" id="2">
        <table class="table table-striped">
            <thead>
                <th>Waktu</th>
                <th>Flow</th>
                <th>Pressure</th>
                <th>Meter Index</th>
            </thead>
            <tbody>
            @foreach($measurements as $measurement)
                <tr>
                    <td>{{$measurement->day}}-{{$measurement->month}}-{{$measurement->year}} &nbsp; {{$measurement->hour}}:00</td>
                    <td>{{$measurement->flow}} m³/jam</td>
                    <td>{{$measurement->pressure}} bar</td>
                    <td>{{$measurement->meter_index}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
      </div>
      <div class="tab-pane" id="3">
        <table class="table table-striped">
            <thead>
                <th>Waktu</th>
                <th>Flow (Rata-rata per-hari)</th>
                <th>Pressure (Rata-rata per-hari)</th>
                <th>Meter Index</th>
            </thead>
            <tbody>
            @foreach($measurements as $index => $measurement)
                @php
                  $datenow = $measurement->day .'-'. $measurement->month .'-'. $measurement->year;
                  if ($index == 1) {
                    $datelatest = $measurement->day .'-'. $measurement->month .'-'. $measurement->year;
                    $flow_temp = $measurement->flow;
                    $pressure_temp = $measurement->pressure;
                    $meterindex_temp = $measurement->meter_index;
                  }
                @endphp

                @if($datenow==$datelatest)
                  @php
                  $flow_temp = $flow_temp + $measurement->flow;
                  $pressure_temp = $pressure_temp + $measurement->pressure;
                  $meterindex_temp = $measurement->meter_index;
                  @endphp
                @else
                  <tr>
                      <td>{{ $datelatest }}</td>
                      <td>{{ number_format($flow_temp/24,2) }} m³/jam</td>
                      <td>{{ number_format($pressure_temp/24,2) }} bar</td>
                      <td>{{ $meterindex_temp }}</td>
                  </tr>
                @endif

                @php
                  $datelatest = $measurement->day .'-'. $measurement->month .'-'. $measurement->year;
                @endphp
            @endforeach
            </tbody>
        </table>
      </div>
    </div>
    </div>
    @endif
    <br /><br />
@stop
