@extends('dashboard')

@section('content')

    <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
            {!! Form::open(['route' => 'logsystems.search','class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
              {!! csrf_field() !!}
              <input class="form-control form-inline pull-right" name="search" placeholder="Search">
            {!! Form::close() !!}
            <h1 class="pull-left"><span class="fa fa-database"></span> Log Sistem</h1>
        </div>
    </div>
    <div class="row raw-margin-top-24">
        <div class="col-md-12">
            @if($logsystems->isEmpty())
                <div class="well text-center">No logsystems found.</div>
            @else
                <table class="table table-striped">
                    <thead>
                        <th>Aktifitas</th>
                        <th width="200px" class="text-right">Aksi</th>
                    </thead>
                    <tbody>
                    @foreach($logsystems as $logsystem)
                        <tr>
                            <td>
                                {{ $logsystem->user->name }} {{ $logsystem->name }} at {{ $logsystem->created_at }} from {{ $logsystem->ipaddress }}
                            </td>
                            <td>
                                <form method="post" action="{!! route('logsystems.destroy', [$logsystem->id]) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this logsystem?')"><i class="fa fa-trash"></i> Delete</button>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="row text-center">
                    {!! $logsystems; !!}
                </div>
            @endif
        </div>
    </div>

@stop
