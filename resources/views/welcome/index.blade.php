<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>{{ env('APP_NAME') }}</title>
    <link rel="stylesheet" href="{{ url('css/bootstrap.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('css/magnific-popup.min.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('css/creative.css') }}" type="text/css">
    <link rel="stylesheet" href="{{ url('css/font-awesome.min.css') }}" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" type="text/css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic" type="text/css">
</head>

<body id="page-top">

    <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">{{ env('APP_NAME') }}</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <a class="page-scroll" href="#about">Tentang Aplikasi</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">Fitur</a>
                    </li>
                    <li>
                      @if (Auth::user())
                      <a class="page-scroll" href="{!! url('dashboard') !!}">Dashboard</a>
                      @else
                      <a class="page-scroll" href="#page-top" data-toggle="modal" data-target="#myModal">Login</a>
                      @endif
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">Contact</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <header>
        <div class="header-content">
            <div class="header-content-inner">
                <h1 id="homeHeading">{{ $contentcopywritings[0]->name }}</h1>
                <hr>
                <p>{{ $contentcopywritings[0]->body }}</p>
                <a href="#about" class="btn btn-primary btn-xl page-scroll">Tentang Aplikasi</a>
            </div>
        </div>
    </header>

    <section class="bg-primary" id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">{{ $contentcopywritings[1]->name }}</h2>
                    <hr class="light" style="border:1px solid;">
                    <p class="text-faded">{{ $contentcopywritings[1]->body }}</p>
                    <a href="#services" class="page-scroll btn btn-default btn-xl sr-button">Fitur</a>
                </div>
            </div>
        </div>
    </section>

    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Fitur &amp; Teknologi</h2>
                    <hr class="primary">
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-institution text-primary sr-icons"></i>
                        <h3>{{ $contenttechs[0]->name }}</h3>
                        <p class="text-muted">{{ $contenttechs[0]->body }}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-cube text-primary sr-icons"></i>
                        <h3>{{ $contenttechs[1]->name }}</h3>
                        <p class="text-muted">{{ $contenttechs[1]->body }}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-tachometer text-primary sr-icons"></i>
                        <h3>{{ $contenttechs[2]->name }}</h3>
                        <p class="text-muted">{{ $contenttechs[2]->body }}</p>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 text-center">
                    <div class="service-box">
                        <i class="fa fa-4x fa-mobile-phone text-primary sr-icons"></i>
                        <h3>{{ $contenttechs[3]->name }}</h3>
                        <p class="text-muted">{{ $contenttechs[3]->body }}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="no-padding" id="portfolio">
        <div class="container-fluid">
            <div class="row no-gutter popup-gallery">
                <div class="col-lg-4 col-sm-6">
                    <a href="http://103.75.237.39/dashboards/" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/1.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Chart logger dari semua alat
                                </div>
                                <div class="project-name">
                                    Dashboard Logger General
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="http://103.75.237.39/dashboards/index.php?mod=0401" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/2.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Chart logger dari area SPAM Lembang
                                </div>
                                <div class="project-name">
                                    Chart SPAM Lembang
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="http://103.75.237.39/dashboards/index.php?mod=0304" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/3.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Chart logger dari area SPAM Ciwidey
                                </div>
                                <div class="project-name">
                                    Chart SPAM Ciwidey
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="http://103.75.237.39/dashboards/index.php?mod=0901" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/4.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Chart logger dari area DW 11 / SPAM Cimahi
                                </div>
                                <div class="project-name">
                                    Chart SPAM Cimahi
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="http://103.75.237.39/dashboards/index.php?mod=0702" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/5.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Chart logger dari area SPAM Cileunyi
                                </div>
                                <div class="project-name">
                                    Chart SPAM Cileunyi
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-lg-4 col-sm-6">
                    <a href="http://103.75.237.39/dashboards/index.php?mod=0601" class="portfolio-box">
                        <img src="img/portfolio/thumbnails/6.jpg" class="img-responsive" alt="">
                        <div class="portfolio-box-caption">
                            <div class="portfolio-box-caption-content">
                                <div class="project-category text-faded">
                                    Chart logger dari area SPAM Kutawaringin
                                </div>
                                <div class="project-name">
                                    Chart SPAM Kutawaringin
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>

    <aside class="bg-dark">
        <div class="container text-center">
            <div class="call-to-action">
                <h2>Login terlebih dahulu untuk masuk ke sistem.</h2>
                <a href="#page-top" data-toggle="modal" data-target="#myModal" class="btn btn-default btn-xl sr-button">Login</a>
            </div>
        </div>
    </aside>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 text-center">
                    <h2 class="section-heading">Kontak Kami</h2>
                    <hr class="primary">
                    <p>Untuk informasi teknis atau inquiry, bisa menghubungi Pengembang &amp; Stakeholder Aplikasi:<br />
                      PDAM Tirta Raharja Kabupaten Bandung<br />
                      PT Jerbee Indonesia, Bandung, Indonesia</p>
                </div>
                <div class="col-lg-4 col-lg-offset-2 text-center">
                    <i class="fa fa-phone fa-3x sr-contact"></i>
                    <p>+62 022 7323768</p>
                </div>
                <div class="col-lg-4 text-center">
                    <i class="fa fa-envelope-o fa-3x sr-contact"></i>
                    <p>info@jerbeeindonesia.com</a></p>
                </div>
            </div>
        </div>
    </section>

    <div id="myModal" class="modal fade" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login / Register</h4>
        </div>
        <div class="modal-body">
          @include('auth.smalllogin')
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
    </div>

    <script src="{{ url('js/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ url('js/bootstrap.min.js') }}"></script>
    <script src="{{ url('js/jquery.easing.min.js') }}"></script>
    <script src="{{ url('js/scrollreveal.min.js') }}"></script>
    <script src="{{ url('js/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ url('js/creative.min.js') }}"></script>

</body>

</html>
