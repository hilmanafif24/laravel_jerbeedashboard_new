@extends('dashboard')

@section('content')

<div class="row">
  <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
    <h1 class="pull-left"><span class="fa fa-map-marker"></span> Ubah DMA</h1>
  </div>
</div>
<div class="row raw-margin-top-24">
  <div class="col-md-6 raw-margin-bottom-24">
    <div>
      {!! Form::model($dma, ['route' => ['dmas.update', $dma->id], 'method' => 'patch']) !!}
      {!! csrf_field() !!}
      {!! method_field('PATCH') !!}

      {!! Form::open(['route' => 'dmas.store']) !!}
      {!! csrf_field() !!}

      @form_maker_object($dma, [
        'id'=> ['type'=>'hidden'],
        'zona'=>['alt_name'=>'Zona','type'=>'relationship','model' =>
        'App\Models\Zona','method'=>'nama_kode'],
        'kode' => ['alt_name'=>'Kode'],
        'name' => ['alt_name'=>'Nama'],
        'polygon' => ['type'=>'hidden'],
        'lat'=>['alt_name'=>'Latitude'],
        'long'=>['alt_name'=>'Longitude'],
        ])

        <div class="form-group">
          <label class="control-label" for="avatar">Peta Area</label><br />(Drag marker untuk mengatur input koordinat)
          <div style="width: 300px; height: 300px; border:1px solid #ccc;">
            {!! Mapper::render() !!}
          </div>
        </div>

        <div class="raw-margin-top-24">
          <a class="btn btn-default pull-left" href="{!! route('dmas.index') !!}">Batal</a>
          <button class="btn btn-primary pull-right" type="submit">Simpan</button>
        </div>
        {!! Form::close() !!}
      </div>
    </div>
  </div>

  @stop
