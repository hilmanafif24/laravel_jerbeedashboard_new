@extends('dashboard')

@section('javascript')
<script src="{{ url('js/multiselect.min.js') }}"></script>
<script type="text/javascript">
jQuery(document).ready(function($) {
   $('#search').multiselect({
        search: {
            left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
            right: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
           },
        fireSearch: function(value) {
            return value.length > 3;
       }
    });
});
</script>

@stop

@section('content')

    <div class="row">
        <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        <h1 class="pull-left"><span class="fa fa-map-marker"></span> Pengelolaan Pelanggan ke DMA</h1>
        </div>
    </div>

    @if($dkd_id)

      <div class="row raw-margin-top-24">
          <div class="col-md-12">
          <div style="width: 300px; height: 200px; border:1px solid #ccc; float:left; margin-right:14px;">
            {!! Mapper::render() !!}
          </div>
          <p><b>Nama DMA:</b> {{ $dma->name }}</p>
          <p><b>Kode DMA:</b> {{ $dma->kode }}</p>
          <p><b>Zona:</b> {{ $dma->zona->name }}</p>
          <p><b>SPAM:</b> {{ $dma->zona->spam->name }}</p>
          <p><b>DKD Saat Ini:</b> {{ $dkd_id }}</p>
          </div>
      </div>

      <br />

      {!! Form::open(['route' => 'dmas.assign']) !!}
      {!! csrf_field() !!}
      <div class="row">
      <div class="col-xs-5">
          <span style="font-weight:bold;">Pelanggan</span>
          <select name="from[]" id="search" class="form-control" size="12" multiple="multiple">
            @foreach($pelanggans as $pelanggan)
              <option value="{{ $pelanggan->pel_no }}">{{ (string) $pelanggan->pel_no }} {{ $pelanggan->pel_nama }}</option>
            @endforeach
          </select>
      </div>

      <div class="col-xs-2">
          <br /><br />
          <button type="button" id="search_rightAll" class="btn btn-block"><i class="fa fa-forward"></i></button>
          <button type="button" id="search_rightSelected" class="btn btn-block"><i class="fa fa-chevron-right"></i></button>
          <button type="button" id="search_leftSelected" class="btn btn-block"><i class="fa fa-chevron-left"></i></button>
          <button type="button" id="search_leftAll" class="btn btn-block"><i class="fa fa-backward"></i></button>
      </div>

      <div class="col-xs-5">
          <span style="font-weight:bold;">Anggota DMA</span>
          <select name="search_to[]" id="search_to" class="form-control" size="12" multiple="multiple">
            @foreach($memberdmas as $memberdma)
              <option value="{{ $memberdma->pel_no }}">{{ (string) $memberdma->pel_no }} {{ $memberdma->pel_nama }}</option>
            @endforeach
          </select>
      </div>
      </div>

      <input type="hidden" name="dma_id" value="{{ $dma->id }}" />

      <br />
      {!! link_to(URL::previous(), 'Back', ['class' => 'btn btn-default pull-left']) !!}
      &nbsp;
      <button class="btn btn-primary" type="submit">Update / Assign</button>
      {!! Form::close() !!}

    @else

    <div class="row raw-margin-top-24">
        <div class="col-md-6">

        <h3>Pilih DKD yang akan dikelola pelanggannya (ke DMA) terlebih dahulu.</h3>

        {!! Form::open(['url' => 'dmas/'.$dma->id, 'method' => 'get' ]) !!}
        {!! csrf_field() !!}
        <input type="hidden" name="dma" value="{{$dma->id}}">
        <label class="control-label" for="dkd_id">DKD :</label><br />
        <select class="form-control" name="dkd_id" id="dkd_id">
          @foreach ($dkds as $dkd)
            <option value="{{$dkd->dkd_kd}}">{{$dkd->dkd_kd}}</option>
          @endforeach
        </select>
        <br />
        <button class="btn btn-primary" type="submit">Lanjut</button>
        {!! Form::close() !!}

        </div>
    </div>

    <br />

    @endif



@stop
