@extends('dashboard')

@section('content')

<div class="row">
  <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
    <h1 class="pull-left"><span class="fa fa-location-arrow"></span> Ubah Zona</h1>
  </div>
</div>
<div class="row raw-margin-top-24">
  <div class="col-md-6 raw-margin-bottom-24">
    <div>
      {!! Form::model($zona, ['route' => ['zonas.update', $zona->id], 'method' => 'patch']) !!}
      {!! csrf_field() !!}
      {!! method_field('PATCH') !!}

      @form_maker_object($zona,[
        'id'=>['type'=>'hidden'],
        'spam'=>[
          'alt_name'=>'Spam',
          'type'=>'relationship',
          'model' => 'App\Models\Spam',
          'method'=>'kode_name',
        ],
        'kode'=>['alt_name'=>'Kode'],
        'name'=>['alt_name'=>'Nama'],
        'polygon'=>['alt_name'=>'Longitude'],
        ])
        <div class="form-group">
          <label class="control-label" for="avatar">Peta Area</label><br />(Drag marker untuk mengatur input koordinat)
          <div style="width: 300px; height: 300px; border:1px solid #ccc;">
           {!! Mapper::render() !!}
         </div>
       </div>


       <div class="raw-margin-top-24">
        <a class="btn btn-default pull-left" href="{!! route('zonas.index') !!}">Batal</a>
        <button class="btn btn-primary pull-right" type="submit">Simpan</button>
      </div>

      {!! Form::close() !!}
    </div>
  </div>
</div>

@stop
