@extends('dashboard')

@section('content')

<header>
 <script src="https://maps.googleapis.com/maps/api/js?libraries=geometry,drawing"></script>
 <script>
   var geocoder;
   var map;
   var polygonArray = [];
   function initialize() {
    map = new google.maps.Map(
     document.getElementById("map_canvas"), {
      center: new google.maps.LatLng(-6.956273, 107.546515),
      zoom: 9,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    var drawingManager = new google.maps.drawing.DrawingManager({
      drawingMode: google.maps.drawing.OverlayType.POLYGON,
      drawingControl: true,
      drawingControlOptions: {
       position: google.maps.ControlPosition.TOP_CENTER,
       drawingModes: [
       google.maps.drawing.OverlayType.POLYGON,
       ]
     },
     polygonOptions: {
      fillColor: '#BCDCF9',
      fillOpacity: 0.5,
      strokeWeight: 2,
      strokeColor: '#57ACF9',
      clickable: false,
      editable: false,
      zIndex: 1
    }
  });
    console.log(drawingManager)
    drawingManager.setMap(map)
    google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
      var dat;
      for (var i = 0; i < polygon.getPath().getLength(); i++) {
       dat += polygon.getPath().getAt(i).toUrlValue(6) + ";";
     }
     document.getElementById('polygon').value=dat.replace('undefined','');
     polygonArray.push(polygon);
   });

  }
  google.maps.event.addDomListener(window, "load", initialize);
</script>
</header>
<div class="row">
  <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
    <h1 class="pull-left"><span class="fa fa-location-arrow"></span> Tambah Zona</h1>
  </div>
</div>
<div class="row raw-margin-top-24">
  <div class="col-md-6 raw-margin-bottom-24">
   {!! Form::open(['route' => 'zonas.store']) !!}
   {{ csrf_field() }}
   @form_maker_table("zonas",[
    'id'=>['type'=>'hidden'],
    'spam_id'=>[
      'alt_name'=>'Spam',
      'type'=>'relationship',
      'model' => 'App\Models\Spam',
      'method'=>'kode_name'],
      'kode'=>[''],
      'name'=>['alt_name'=>'Nama'],
      ])
      <div class="form-group polygon">
        <label for="polygon">Polygon</label>
        <input class="form-control" type="text" name="polygon" id="polygon" required placeholder="Polygon">
      </div>
      <div id="map_canvas" style=" border: 2px solid #3872ac;">
      </div>
      <div class="raw-margin-top-24">
        <a class="btn btn-default pull-left" href="{!! route('zonas.index') !!}">Batal</a>
        <button class="btn btn-primary pull-right" type="submit">Buat</button>
      </div>
    </div>
  </div>
  {!! Form::close() !!}
  @stop
