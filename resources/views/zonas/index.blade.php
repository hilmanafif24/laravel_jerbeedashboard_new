@extends('dashboard')

@section('content')
<div class="row">
    <div class="col-md-12" style="background:#eee; border-radius:0 0 14px 14px; margin:4px;">
        {!! Form::open(['route' => 'zonas.search','class' => 'pull-right raw-margin-top-24 raw-margin-left-24']) !!}
          {!! csrf_field() !!}
          <input class="form-control form-inline pull-right" name="search" placeholder="Search">
        {!! Form::close() !!}
        <a class="btn btn-primary pull-right" style="margin-top: 25px" href="{!! route('zonas.create') !!}">Tambah Baru</a>
        <h1 class="pull-left"><span class="fa fa-location-arrow"></span> Kelola Zona</h1>
    </div>
</div>
    <div class="row raw-margin-top-24">
        <div class="col-md-12">
            @if($zonas->isEmpty())
                <div class="well text-center">Tidak ada Zona yang ditemukan.</div>
            @else
                <table class="table table-striped">
                    <thead>
                        <th>Nama</th>
                        <th>Kode</th>
                        <th width="140px" class="text-right">Aksi</th>
                    </thead>
                    <tbody>
                    @foreach($zonas as $zona)
                        <tr>
                            <td>
                                {{ $zona->name }}
                            </td>

                            <td>
                                {{ $zona->kode }}
                            </td>

                            <td>
                                <form method="post" action="{!! route('zonas.destroy', [$zona->id]) !!}">
                                    {!! csrf_field() !!}
                                    {!! method_field('DELETE') !!}
                                    <button class="btn btn-danger btn-xs pull-right" type="submit" onclick="return confirm('Are you sure you want to delete this zona?')"><i class="fa fa-trash"></i> Hapus</button>
                                </form>
                                <!-- <a class="btn btn-warning btn-xs" href="{!! route('zonas.show', [$zona->id]) !!}"><i class="fa fa-search"></i> Detail</a>-->

                                <a class="btn btn-warning btn-xs" href="{!! route('zonas.edit', [$zona->id]) !!}"><i class="fa fa-pencil"></i> Ubah</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="row text-center">
                    {!! $zonas; !!}
                </div>
            @endif
        </div>
    </div>

@stop
