<?php

namespace App\Services;

use App\Models\Station;
use Illuminate\Support\Facades\Schema;

class StationService
{
    /**
     * Service Model
     *
     * @var Model
     */
    public $model;

    /**
     * Pagination
     *
     * @var integer
     */
    public $pagination;

    /**
     * Service Constructor
     *
     * @param Station $station
     */
    public function __construct(Station $station)
    {
        $this->model        = $station;
        $this->pagination   = env('PAGINATION', 25);
    }

    /**
     * All Model Items
     *
     * @return array
     */
    public function all()
    {
        return $this->model->all();
    }

    /**
     * Paginated items
     *
     * @return LengthAwarePaginator
     */
    public function paginated()
    {
        return $this->model->orderBy('name')->paginate($this->pagination);
    }

    /**
     * Search the model
     *
     * @param  mixed $payload
     * @return LengthAwarePaginator
     */
    public function search($payload)
    {
        $query = $this->model->orderBy('created_at', 'desc');
        $query->where('id', 'LIKE', '%'.$payload.'%');

        $columns = Schema::getColumnListing('stations');

        foreach ($columns as $attribute) {
            $query->orWhere($attribute, 'LIKE', '%'.$payload.'%');
        };

        return $query->paginate($this->pagination)->appends([
            'search' => $payload
        ]);
    }

    /**
     * Create the model item
     *
     * @param  array $payload
     * @return Model
     */
    public function create($payload)
    {
        if ($payload['spam_id']==null) {
            $payload['spam_id']=0;
        } else {
            $spam_id = $payload['spam_id'];
        }
        if ($payload['zona_id']==null) {
            $payload['zona_id']=0;
        } else {
            $zona_id = $payload['zona_id'];
        }
        if ($payload['dma_id']==null) {
             $payload['dma_id']=0;
        } else {
            $dma_id = $payload['dma_id'];
        }
        $payload['v_spam_kode_id'] = sprintf("%'.02d",$payload['spam_id']).sprintf("%'.02d",$payload['zona_id']).sprintf("%'.02d",$payload['dma_id']);
        return $this->model->create($payload);
    }

    /**
     * Find Model by ID
     *
     * @param  integer $id
     * @return Model
     */
    public function find($id)
    {
        return $this->model->find($id);
    }

    /**
     * Model update
     *
     * @param  integer $id
     * @param  array $payload
     * @return Model
     */
    public function update($id, $payload)
    {
        return $this->find($id)->update($payload);
    }

    /**
     * Destroy the model
     *
     * @param  integer $id
     * @return bool
     */
    public function destroy($id)
    {
        return $this->model->destroy($id);
    }
}
