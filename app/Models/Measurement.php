<?php

namespace App\Models;
use App\Models\Device;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model
{
    public $table = "measurements";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
		'device_id',
		'date',
		'value',

    ];

    public static $rules = [
        // create rules
    ];


   

}
