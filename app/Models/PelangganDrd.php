<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan;

class PelangganDrd extends Model
{
  public $table = "tm_drd_awal";
  public $primaryKey = "pel_no";

  public function pelanggan()
  {
      return $this->belongsTo(Pelanggan::class,"pel_no","pel_no");
  }

}
