<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;
use App\Models\PelangganKotapelayanan;
use App\Models\PelangganKondisipasang;
use App\Models\PelangganGolongan;
use App\Models\PelangganDrd;

class Pelanggan extends Model
{
  public $table = "tm_pelanggan";
  public $primaryKey = "pel_no";
  public $incrementing = false;

  public function kotapelayanan()
  {
      return $this->belongsTo(PelangganKotapelayanan::class,"kp_kode","kp_kode");
  }

  public function golongan()
  {
      return $this->belongsTo(PelangganGolongan::class,"gol_kode","gol_kode");
  }

  public function kondisipasang()
  {
      return $this->belongsTo(PelangganKondisipasang::class,"kps_ket","kps_ket");
  }

  public function drd()
  {
      return $this->hasMany(PelangganDrd::class,"pel_no","pel_no");
  }

}
