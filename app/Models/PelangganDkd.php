<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan;

class PelangganDkd extends Model
{
  public $table = "tr_dkd";
  public $primaryKey = "dkd_kd";

  public function pelanggans()
  {
      return $this->hasMany(Pelanggan::class,"dkd_kd","dkd_kd");
  }

}
