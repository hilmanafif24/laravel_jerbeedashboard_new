<?php
namespace App\Models;
use App\Models\Station;
use App\Models\Brand;
use App\Models\Measurement;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    public $table = "devices";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
		'name',
		'station_id',
		'brand_id',
		'label',
		'unit',

    ];

    public static $rules = [
        // create rules
    ];

    // Device
    public function brand()
    {
        return $this->belongsTo(Brand::class, 'brand_id');
    }
    public function station()
    {
        return $this->belongsTo(Station::class, 'station_id');
    }
   

}
