<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Station;

class Location extends Model
{
    public $table = "locations";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        		'id',
		'name',

    ];

    public static $rules = [
        // create rules
    ];

    public function stations()
    {
        return $this->hasMany(Station::class);
    }
}
