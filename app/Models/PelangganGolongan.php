<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan;

class PelangganGolongan extends Model
{
  public $table = "tr_gol";
  public $primaryKey = "gol_kode";

  public function pelanggans()
  {
      return $this->hasMany(Pelanggan::class,"gol_kode","gol_kode");
  }

}
