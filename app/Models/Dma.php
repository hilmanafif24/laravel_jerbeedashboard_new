<?php

namespace App\Models;

use App\Models\Pelanggan;
use App\Models\Zona;
use Illuminate\Database\Eloquent\Model;
use App\Models\Station;

class Dma extends Model
{
    public $table = "dmas";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
        'zona_id',
        'kode',
        'name',
        'station',
        'polygon',
        'lat',
        'long'
    ];

    public static $rules = [
        // create rules
        // 'zona_id' => 'required',
        // 'kode' => 'required|string:2',
        // 'name' => 'required',
    ];

    // Dma
    public function pelanggans()
    {
        return $this->belongsToMany(Pelanggan::class, 'pelanggan_dmas',
            'dma_id', 'pel_no');
    }

    public function zona()
    {
        return $this->belongsTo(Zona::class);
    } 
  
    public function stations()
    {
        return $this->hasMany(Station::class,'station');
    }
}

