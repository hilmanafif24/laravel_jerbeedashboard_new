<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Spam;
use app\Models\Dma;
use DB;
use App\Models\Station;

class Zona extends Model
{
    public $table = "zonas";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
		'spam_id',
		'kode',
		'name',
		'polygon',
        'lat',
        'long',

    ];

    public static $rules = [
        // create rules
        // 'spam_id'=>'required|numeric',
    	'kode'=>'required',
    	'name'=>'required',
    ];

    // Zona
    public function spam(){
        return $this->belongsTo(Spam::class);
    }

    public function dmas(){
        return $this->hasMany(Dma::class);
    }
    
    public function nama_kode()
    {
        return $this->select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
    }
    public function stations(){
        return $this->hasMany(Station::class);
    }

}
