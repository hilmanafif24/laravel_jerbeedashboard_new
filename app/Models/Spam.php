<?php

namespace App\Models;
use DB;
use App\Models\Zona;
use Illuminate\Database\Eloquent\Model;
use App\Models\Station;


class Spam extends Model
{
    public $table = "spams";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
        'kode',
        'name',
        'polygon',
    ];

    public static $rules = [
        // create rules
        'kode'=>'required',
        'name'=>'required'
    ];

    public function kode_name()
    {
        return $this->select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
    }

    public function zonas()
    {
        return $this->hasMany(Zona::class);
    }
    // 
    public function stations()
    {
        return $this->hasMany(Station::class);
    }
}
