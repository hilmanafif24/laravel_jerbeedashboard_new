<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan;

class PelangganKotapelayanan extends Model
{
  public $table = "tr_kota_pelayanan";
  public $primaryKey = "kp_kode";

  public function pelanggans()
  {
      return $this->hasMany(Pelanggan::class,"kp_kode","kp_kode");
  }

}
