<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Pelanggan;

class PelangganKondisipasang extends Model
{
  public $table = "tr_kondisi_ps";
  public $primaryKey = "kps_ket";

  public function pelanggans()
  {
      return $this->hasMany(Pelanggan::class,"kps_ket","kps_ket");
  }

}
