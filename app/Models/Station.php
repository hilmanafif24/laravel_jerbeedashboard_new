<?php
namespace App\Models; 
use Illuminate\Database\Eloquent\Model;
use App\Models\Device;
use App\Models\Spam;
use App\Models\Zona;
use App\Models\Dma;
//nambah model brand dan location
use DB;

class Station extends Model
{
    public $table = "stations";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
        'brand_id',
        'location_id',
        'name',
        'instalation_date',
        'lat',
        'long',
        'v_spam_kode_id',
        'kode',
        'spam_id',
        'zona_id',
        'dma_id',
        'kode',
        'chart_url',
        'data_url',

    ];

    public static $rules = [
        // create rules
    ];

    // Station 
    public function spam()
    {
        return $this->belongsTo(Spam::class, 'spam_id');
    }

    public function zona()
    {
        return $this->belongsTo(Zona::class, 'zona_id');
    }
    public function dma()
    {
        return $this->belongsTo(Dma::class, 'dma_id');
    }
    public function devices()
    {
        return $this->hasMany(Device::class, 'brand_id');
    }

    public function nama_kode()
    {
        return $this->select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
    }



}
