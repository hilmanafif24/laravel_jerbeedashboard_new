<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

use App\Models\Device;


class Brand extends Model implements StaplerableInterface {
    use EloquentTrait;
    public $table = "brands";

    public $primaryKey = "id";

    public $timestamps = true;

    public $fillable = [
        'id',
		'name',
        'phone',
        'address',
        'logo',

    ];

    public function __construct(array $attributes = array()) {
        $this->hasAttachedFile('logo', [
            'styles' => [
            'medium' => '200x200',
            'thumb' => '100x100',   
            ],

            'url' => 'upload/brand/:attachment/:id/:style/:filename',
            'default_url' => '/img/your_logo2.png'
        ]);

        parent::__construct($attributes);
    }

    public static $rules = [
        // create rules
        'name' => 'required',
        'phone' => 'required',
        'address' => 'required',
        'logo' => 'required',
    ];

    // Brand 
    public function devices()
    {
        return $this->hasMany(Device::class);
    } 

}
