<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DmaService;
use App\Http\Requests\DmaCreateRequest;
use App\Http\Requests\DmaUpdateRequest;
use Mapper;
use App\Models\Pelanggan;
use App\Models\PelangganDkd;

class DmasController extends Controller
{
    public function __construct(DmaService $dmaService)
    {
        $this->service = $dmaService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $dmas = $this->service->paginated();
        return view('dmas.index')->with('dmas', $dmas);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $dmas = $this->service->search($request->search);
        return view('dmas.index')->with('dmas', $dmas);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
        return view('dmas.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\DmaCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DmaCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('dmas.index'))->with('message', 'Successfully created');
            //return redirect(route('dmas.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('dmas.index'))->with('message', 'Failed to create');
    }

    // Digunakan untuk manajemen pelanggan-DMA
    public function show($id, Request $request)
    {
        $dma = $this->service->find($id);
        if ($dma->lat != null) {
          Mapper::map($dma->lat,$dma->long, [
          'draggable' => false,
          ]);
        } else {
          Mapper::map(-6.9809279,107.486755, [
          'draggable' => false,
          ]);
        }

        // Kalau belum milih DKD, siapkan dropdown DKD
        // Sementara dari groupby tm_pelanggan (banyak row bgt), nanti dicari sumber dkd yg lebih friendly
        if($request->dkd_id == null) {

          $dkds = PelangganDkd::select('dkd_kd')->groupBy('dkd_kd')->orderBy('dkd_kd','DESC')->get();
          $dkd_id = null;
          return view('dmas.show', compact('dma','dkd_id','dkds'));

        } else {

          $dkd_id = $request->dkd_id;
          $pelanggans = Pelanggan::where('dkd_kd','=',$dkd_id)->get();
          $memberdmas = $dma->pelanggans()->get();

          return view('dmas.show', compact('dma','pelanggans','memberdmas','dkd_id'));

        }



    }

    /**
     * Show the form for editing the dma.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $dma = $this->service->find($id);
        if ($dma->lat != null) {
        Mapper::map(-6.849464713343289, 107.58418970979005)->polygon([ ['latitude' => -6.7975804862857885, 'longitude' => 107.53203392028809],
            ['latitude' => -6.799114575998445, 'longitude' => 107.55795478820801],
            ['latitude' => -6.802353193752624, 'longitude' => 107.57357597351074],
            ['latitude' => -6.827068240877113, 'longitude' => 107.57306098937988],
            ['latitude' => -6.836272139404449, 'longitude' => 107.53237724304199],
            ['latitude' => -6.81854595477611, 'longitude' => 107.51143455505371]],
            ['strokeColor' => '#500000', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
      } else {
        Mapper::map(-6.849464713343289, 107.58418970979005)->polygon([ ['latitude' => -6.7975804862857885, 'longitude' => 107.53203392028809],
            ['latitude' => -6.799114575998445, 'longitude' => 107.55795478820801],
            ['latitude' => -6.802353193752624, 'longitude' => 107.57357597351074],
            ['latitude' => -6.827068240877113, 'longitude' => 107.57306098937988],
            ['latitude' => -6.836272139404449, 'longitude' => 107.53237724304199],
            ['latitude' => -6.81854595477611, 'longitude' => 107.51143455505371]],
            ['strokeColor' => '#500000', 'strokeOpacity' => 0.3, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
      }

        $dma = $this->service->find($id);
        return view('dmas.edit')->with('dma', $dma);
    }

    /**
     * Update the dmas in storage.
     *
     * @param  \Illuminate\Http\DmaUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DmaUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            //return back()->with('message', 'Successfully updated');
            return redirect(route('dmas.index'))->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    public function assign(Request $request)
    {

        $dma = $this->service->find($request->dma_id);

        $memberdmas = $request->input('search_to');
        //$memberdmas = implode(',', $memberdmas);
        $result = $dma->pelanggans()->detach();
        $result = $dma->pelanggans()->attach($memberdmas);

        //return back()->with('message', 'Failed to update');
        return back()->with('message', 'Successfully updated');
    }

    /**
     * Remove the dmas from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('dmas.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('dmas.index'))->with('message', 'Failed to delete');
    }

}
