<?php
 
namespace App\Http\Controllers; 

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Spam;
use App\Models\Zona;
use App\Models\Dma;

use App\Models\PelangganDrd;
use App\Models\Measurement;

use DB;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        return view('public_dashboard.dslide');
    }

    public function detail(Request $request)
    {

    	// Siapkan dropdown untuk filter
        $spams = Spam::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
        $zonas = Zona::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
        $dmas = Dma::select('name', DB::raw("CONCAT(`kode`, ' - ', `name`) AS name"),'id')->get();
        // Inisiasi/memastikan saat dropdown belum dipilih nilai harus 0
        $spamselect = collect(new Spam);
        $spamselect->id = null;
        $zonaselect = collect(new Zona);
        $zonaselect->id = null;
        $dmaselect = collect(new Dma);
        $dmaselect->id = null;

        // Jika dropdown DMA dipilih (masih rancu, logger.. KUDU KOREKSI!)
        if($request->dma_id) {
          $dmaselect = DMA::find($request->dma_id);
          // Mining dari tabel measurements data DMA tersebut
          $measurements = DB::select("SELECT COALESCE(YEAR(m.date), '0') as year,
					COALESCE(MONTH(m.date) - 1, '0') as month,
					COALESCE(DAY(m.date), '0') as day,
					COALESCE(HOUR(m.date), '0') as hour,
					COALESCE(MINUTE(m.date),'0') as minute,
					ROUND(CASE WHEN d.label='Flow' THEN m.value END, 2) AS flow,
					ROUND(CASE WHEN d.label='Pressure' THEN m.value END, 2) AS pressure,
					m.meter_index
					FROM loggers l LEFT JOIN devices d ON l.id = d.station_id LEFT JOIN measurements m ON d.id=m.device_id
					WHERE l.v_spam_kode_id = '".$request->dma_id."' ORDER BY date DESC, device_id LIMIT 544;");
          $pressures = "";
          $flows = "";
          foreach ($measurements as $index => $data) {
            // Jika ada value pressure (brarti row pressure)
            if($data->pressure){
              $pressures = $pressures .  "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->pressure."],";
              // Masukan flow & meter (dari temp sebelumnya) ke row pressure
              $data->flow = $flow_temp;
              $data->meter_index = $meterindex_temp;
            } else {
              // Jika tidak ada value pressure (brarti row flow)
            	$flows = $flows . "[Date.UTC(".$data->year.",".$data->month.",".$data->day.",".$data->hour.",".$data->minute."),".$data->flow."],";
              // Simpan flow & meter ke temp untuk diinject ke row pressure di iterasi berikutnya
              $flow_temp = $data->flow;
              $meterindex_temp = $data->meter_index;
              // Hapus row flow yang ini/sekarang
              unset($measurements[$index]);
            }
          }
          // Trim/hilangkan koma sisa terakhir
          $pressures = rtrim($pressures,", ");
          $flows = rtrim($flows,", ");

          return view('public_dashboard.dslide_detail',compact('spams','spamselect', 'zonas','zonaselect','dmas','dmaselect','pressures','flows','measurements'));
        }

        return view('public_dashboard.dslide_detail',compact('spams','spamselect','zonas','zonaselect','dmas','dmaselect'));
    }

    public function detail_nrw(Request $request){
    	return view('public_dashboard.dslide_detail_nrw');
    }
}
