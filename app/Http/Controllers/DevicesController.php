<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DeviceService;
use App\Http\Requests\DeviceCreateRequest;
use App\Http\Requests\DeviceUpdateRequest;
use App\Models\Station;
use App\Models\Brand;

class DevicesController extends Controller
{
    public function __construct(DeviceService $deviceService)
    {
        $this->service = $deviceService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $devices = $this->service->paginated();
        return view('devices.index')->with('devices', $devices);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $devices = $this->service->search($request->search);
        return view('devices.index')->with('devices', $devices);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('devices.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\DeviceCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DeviceCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('devices.index'))->with('message', 'Successfully created');
            //return redirect(route('devices.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('devices.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the device.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $device = $this->service->find($id);
        return view('devices.show')->with('device', $device);
    }

    /**
     * Show the form for editing the device.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)       
    {
        $device = $this->service->find($id);
        $stations = Station::select('name','id')->get(); 
        $brands = Brand::select('name','id')->get();
        //return view('devices.edit')->with('device', $device, $stations);
        return view('devices.edit', compact('device', 'stations','brands'));
    }

    /**
     * Update the devices in storage.
     *
     * @param  \Illuminate\Http\DeviceUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(DeviceUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            //return back()->with('message', 'Successfully updated');
            return redirect(route('devices.index'))->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the devices from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('devices.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('devices.index'))->with('message', 'Failed to delete');
    }
}
