<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\StationService;
use App\Http\Requests\StationCreateRequest;
use App\Http\Requests\StationUpdateRequest;
use Mapper;

class StationsController extends Controller
{
    public function __construct(StationService $stationService)
    {
        $this->service = $stationService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $stations = $this->service->paginated();
        return view('stations.index')->with('stations', $stations);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $stations = $this->service->search($request->search);
        return view('stations.index')->with('stations', $stations);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       Mapper::map(-6.9809279,107.486755, [
        'draggable' => true,
        'eventDragEnd' =>
        'document.getElementById("Lat").value=event.latLng.lat();
        document.getElementById("Long").value=event.latLng.lng();'
    ]);
       return view('stations.create');
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\StationCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StationCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('stations.index'))->with('message', 'Successfully created');
            //return redirect(route('stations.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('stations.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the station.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $station = $this->service->find($id);
        return view('stations.show')->with('station', $station);
    }

    /**
     * Show the form for editing the station.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $station = $this->service->find($id);
        if ($station->lat != null) {
          Mapper::map($station->lat,$station->long, [
            'draggable' => true,
            'eventDragEnd' =>
            'document.getElementById("Lat").value=event.latLng.lat();
            document.getElementById("Long").value=event.latLng.lng();'
        ]);
      } else  Mapper::map(-6.9809279,107.486755, [
        'draggable' => true,
        'eventDragEnd' =>
        'document.getElementById("Lat").value=event.latLng.lat();
        document.getElementById("Long").value=event.latLng.lng();'
    ]);
      return view('stations.edit')->with('station', $station);
  }

    /**
     * Update the stations in storage.
     *
     * @param  \Illuminate\Http\StationUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StationUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            //return back()->with('message', 'Successfully updated');
            return redirect(route('stations.index'))->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the stations from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('stations.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('stations.index'))->with('message', 'Failed to delete');
    }
}
