<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\BrandService;
use App\Http\Requests\BrandCreateRequest;
use App\Http\Requests\BrandUpdateRequest;

class BrandsController extends Controller
{
    public function __construct(BrandService $brandService)
    {
        $this->service = $brandService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $brands = $this->service->paginated();
        return view('brands.index')->with('brands', $brands);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $brands = $this->service->search($request->search);
        return view('brands.index')->with('brands', $brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('brands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\BrandCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BrandCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('brands.index'))->with('message', 'Successfully created');
            //return redirect(route('brands.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('brands.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the brand.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $brand = $this->service->find($id);
        return view('brands.show')->with('brand', $brand);
    }

    /**
     * Show the form for editing the brand.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $brand = $this->service->find($id);
        return view('brands.edit')->with('brand', $brand);
    }

    /**
     * Update the brands in storage.
     *
     * @param  \Illuminate\Http\BrandUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BrandUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            //return back()->with('message', 'Successfully updated');
            return redirect(route('brands.index'))->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the brands from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('brands.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('brands.index'))->with('message', 'Failed to delete');
    }
}
