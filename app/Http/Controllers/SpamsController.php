<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\SpamService;
use App\Http\Requests\SpamCreateRequest;
use App\Http\Requests\SpamUpdateRequest;
use Mapper;

class SpamsController extends Controller
{
    public function __construct(SpamService $spamService)
    {
        $this->service = $spamService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $spams = $this->service->paginated();
        return view('spams.index')->with('spams', $spams);
    }

    /**
     * Display a listing of the resource searched.
     *
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request)
    {
        $spams = $this->service->search($request->search);
        return view('spams.index')->with('spams', $spams);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    //   Mapper::map(-6.9809279,107.486755, [
    //     'draggable' => true,
    //     'eventDragEnd' =>
    //     'document.getElementById("Lat").value=event.latLng.lat();
    //     document.getElementById("Long").value=event.latLng.lng();'
    // ]);

        Mapper::map(-6.9809279,107.486755)->polygon([['latitude' => -6.9809279, 'longitude' => 107.486755], ['latitude' => 107.486760, 'longitude' => 107.486770]], ['strokeColor' => '#000000', 'strokeOpacity' => 0.1, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
        Mapper::polygon([['latitude' => -6.9809279, 'longitude' => 107.486755], ['latitude' => -6.9809280, 'longitude' => 107.486775]], ['editable' => 'true']);
        
      return view('spams.create');
  }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\SpamCreateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SpamCreateRequest $request)
    {
        $result = $this->service->create($request->except('_token'));

        if ($result) {
            return redirect(route('spams.index'))->with('message', 'Successfully created');
            //return redirect(route('spams.edit', ['id' => $result->id]))->with('message', 'Successfully created');
        }

        return redirect(route('spams.index'))->with('message', 'Failed to create');
    }

    /**
     * Display the spam.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $spam = $this->service->find($id);
        return view('spams.show')->with('spam', $spam);
    }

    /**
     * Show the form for editing the spam.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $spam = $this->service->find($id);
        if ($spam->lat != null) {
          /*
          Mapper::map($spam->lat,$spam->long, [
            'draggable' => true,
            'eventDragEnd' =>
            'document.getElementById("Lat").value=event.latLng.lat();
             document.getElementById("Long").value=event.latLng.lng();'
          ]);
          */
        //   $data_polygon = array(
        //     ['latitude' => -6.7975804862857885, 'longitude' => 107.53203392028809], 
        //     ['latitude' => -6.799114575998445, 'longitude' => 107.55795478820801],
        //     ['latitude' => -6.802353193752624, 'longitude' => 107.57357597351074], 
        //     ['latitude' => -6.827068240877113, 'longitude' => 107.57306098937988],
        //     ['latitude' => -6.836272139404449, 'longitude' => 107.53237724304199], 
        //     ['latitude' => -6.81854595477611, 'longitude' => 107.51143455505371]
        // );
          Mapper::map(-6.849464713343289, 107.58418970979005)->polygon([ ['latitude' => -6.7975804862857885, 'longitude' => 107.53203392028809], 
            ['latitude' => -6.799114575998445, 'longitude' => 107.55795478820801],
            ['latitude' => -6.802353193752624, 'longitude' => 107.57357597351074], 
            ['latitude' => -6.827068240877113, 'longitude' => 107.57306098937988],
            ['latitude' => -6.836272139404449, 'longitude' => 107.53237724304199], 
            ['latitude' => -6.81854595477611, 'longitude' => 107.51143455505371]],
            ['strokeColor' => '#000000', 'strokeOpacity' => 0.3, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);
      } else {
          /*
          Mapper::map(-6.9809279,107.486755, [
            'draggable' => true,
            'eventDragEnd' =>
            'document.getElementById("Lat").value=event.latLng.lat();
             document.getElementById("Long").value=event.latLng.lng();'
          ]);
          */
        //   $data_polygon = array(
        //     ['latitude' => -6.7975804862857885, 'longitude' => 107.53203392028809], ['latitude' => -6.799114575998445, 'longitude' => 107.55795478820801],
        //     ['latitude' => -6.802353193752624, 'longitude' => 107.57357597351074], ['latitude' => -6.827068240877113, 'longitude' => 107.57306098937988],
        //     ['latitude' => -6.836272139404449, 'longitude' => 107.53237724304199], ['latitude' => -6.81854595477611, 'longitude' => 107.51143455505371]
        // );
          Mapper::map(-6.849464713343289, 107.58418970979005)->polygon([ ['latitude' => -6.7975804862857885, 'longitude' => 107.53203392028809], 
            ['latitude' => -6.799114575998445, 'longitude' => 107.55795478820801],
            ['latitude' => -6.802353193752624, 'longitude' => 107.57357597351074], 
            ['latitude' => -6.827068240877113, 'longitude' => 107.57306098937988],
            ['latitude' => -6.836272139404449, 'longitude' => 107.53237724304199], 
            ['latitude' => -6.81854595477611, 'longitude' => 107.51143455505371]],
            ['strokeColor' => '#000000', 'strokeOpacity' => 0.3, 'strokeWeight' => 2, 'fillColor' => '#FFFFFF']);

      }
      return view('spams.edit')->with('spam', $spam);
  }

    /**
     * Update the spams in storage.
     *
     * @param  \Illuminate\Http\SpamUpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SpamUpdateRequest $request, $id)
    {
        $result = $this->service->update($id, $request->except('_token'));

        if ($result) {
            //return back()->with('message', 'Successfully updated');
            return redirect(route('spams.index'))->with('message', 'Successfully updated');
        }

        return back()->with('message', 'Failed to update');
    }

    /**
     * Remove the spams from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = $this->service->destroy($id);

        if ($result) {
            return redirect(route('spams.index'))->with('message', 'Successfully deleted');
        }

        return redirect(route('spams.index'))->with('message', 'Failed to delete');
    }
}
