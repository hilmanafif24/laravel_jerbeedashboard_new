<?php
use App\Models\Station;
use Illuminate\Database\Seeder;

class StationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

      Station::create([
            'id'=>'1',
		'brand_id'=>'1',
		'location_id'=>'1',
		'name'=>'cisarua station',
		'instalation_date'=>'2017-12-30 11:17:00',
		'lat'=>'',
		'long'=>'',
		'v_spam_kode_id'=>'1',
		'kode'=>'1',
		'chart_url'=>'http:\\chart_url',
		'data_url'=>'http:\\data_url',

        ]);
      Station::create([
            'id'=>'2',
		'brand_id'=>'2',
		'location_id'=>'2',
		'name'=>'cisarua station2',
		'instalation_date'=>'2017-12-30 11:18:00',
		'lat'=>'',
		'long'=>'',
		'v_spam_kode_id'=>'2',
		'kode'=>'2',
		'chart_url'=>'http:\\chart_url',
		'data_url'=>'http:\\data_url',

        ]);
      Station::create([
            'id'=>'3',
		'brand_id'=>'3',
		'location_id'=>'3',
		'name'=>'cisarua station3',
		'instalation_date'=>'2017-12-30 11:19:00',
		'lat'=>'',
		'long'=>'',
		'v_spam_kode_id'=>'3',
		'kode'=>'3',
		'chart_url'=>'http:\\chart_url',
		'data_url'=>'http:\\data_url',

        ]);
    }
}
