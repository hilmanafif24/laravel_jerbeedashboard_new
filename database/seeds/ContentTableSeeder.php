<?php

use App\Models\Content;
use Illuminate\Database\Seeder;

class ContentTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      Content::create([
          'name' => 'Integrasi Logger - LabVIEW - Billing/MMR - GIS',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Aplikasi ini merupakan pengembangan Project Dashboard/Monitoring Logger, mengintegrasikan berbagai data antara lain: data logger dari SCADA/logger - data dan visualisasi LabVIEW - data pelanggan dari MMR/Billing - data geografis dari GIS..',
          'user_id' => 1,
          'category_id' => 1,
      ]);
      Content::create([
          'name' => 'Integrasi Logger - LabVIEW - Billing/MMR - GIS',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Aplikasi ini merupakan pengembangan Project Dashboard/Monitoring Logger, mengintegrasikan berbagai data antara lain: data logger dari SCADA/logger - data dan visualisasi LabVIEW - data pelanggan dari MMR/Billing - data geografis dari GIS.',
          'user_id' => 1,
          'category_id' => 1,
      ]);
      Content::create([
          'name' => 'Basis Data Induk',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Salah satu inisiatif integrasi dan unifikasi data di PDAM Tirta Raharja.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Interoperabilitas',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Mencoba menggabungkan berbagai feed data dari berbagai sistem.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Dashboard',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Olah visualisasi data untuk review dan pengambilan keputusan.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Online',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Dideploy online, bisa diakses dari mana saja.',
          'user_id' => 1,
          'category_id' => 2,
      ]);
      Content::create([
          'name' => 'Database Kepegawaian',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Bio, Dokumen Legal, Struktur Organisasi',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'CV Generator',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Karir &amp; Curriculum Vitae',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Kehadiran',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Absen, Liburan &amp; Cuti',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Payroll',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Payroll',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Task Control',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Todo List, Surat Tugas, Jobe Desc, KPI ',
          'user_id' => 1,
          'category_id' => 3,
      ]);
      Content::create([
          'name' => 'Sistem Asset',
          'description' => ' ',
          'quote' => ' ',
          'offline_writer_id' => 0,
          'offline_writer' => ' ',
          'topic_id' => 0,
          'status' => 'PUBLISHED',
          'body' => 'Pengadaan &amp; Inventaris',
          'user_id' => 1,
          'category_id' => 3,
      ]);
    }
}
