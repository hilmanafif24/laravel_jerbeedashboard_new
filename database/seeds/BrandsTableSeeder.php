<?php

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         //
        Brand::create([
			'name' => 'Crystal XP2i',
            'phone' => '0215502122',
            'address' => 'Jakarta',

            'logo' => '',
        ]);

        Brand::create([
			'name' => 'ROTOMASS',
            'phone' => '0215502123',
            'address' => 'Jakarta',

            'logo' => '',
        ]);

        Brand::create([
			'name' => 'HOBO',
            
            'phone' => '021550214',
            'address' => 'Bandung',
            'logo' => '',
        ]);
    }
}
