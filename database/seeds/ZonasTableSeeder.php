<?php
use App\Models\Zona;
use Illuminate\Database\Seeder;

class ZonasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Zona::create([
            'id' => '1',
            'spam_id' => '1',
            'kode' => '01',
            'name' => 'Cisarua1',
            'polygon'=>''
        ]);

        Zona::create([
            'id' => '2',
            'spam_id' => '1',
            'kode' => '02',
            'name' => 'Cisarua2',
            'polygon'=>''
        ]);
        Zona::create([
            'id' => '3',
            'spam_id' => '1',
            'kode' => '03',
            'name' => 'Cisarua3',
            'polygon'=>''
        ]);
    }
}
