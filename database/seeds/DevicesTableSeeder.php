<?php

use App\Models\Device;
use Illuminate\Database\Seeder;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         //
        Device::create([
        	'name' => 'Device1',
        	'station_id' => '1',
        	'brand_id' => '1',
        	'measurement_id' => '1',
        	'label' => 'Label1',
        	'unit' => 'Unit1',
        ]);

        Device::create([
        	'name' => 'Device2',
        	'station_id' => '2',
        	'brand_id' => '2',
        	'measurement_id' => '2',
        	'label' => 'Label2',
        	'unit' => 'Unit2',
        ]);

        Device::create([
        	'name' => 'Device3',
        	'station_id' => '3',
        	'brand_id' => '3',
        	'measurement_id' => '3',
        	'label' => 'Label3',
        	'unit' => 'Unit3',
        ]);
    }
}
