<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CompanyTableSeeder::class);

        $this->call(CategoryTableSeeder::class);
        $this->call(ContentTableSeeder::class);

        $this->call(SpamsTableSeeder::class);
        $this->call(ZonasTableSeeder::class);
        $this->call(DmasTableSeeder::class);
        $this->call(BrandsTableSeeder::class);
        $this->call(LocationTableSeeder::class);
        $this->call(StationTableSeeder::class);
        $this->call(DevicesTableSeeder::class);

        Model::reguard();
    }
}
