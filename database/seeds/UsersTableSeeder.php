<?php

use App\Models\User;
use App\Models\UserMeta;
use App\Services\UserService;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $service = app(UserService::class);

        if (!User::where('name', 'Admin')->first()) {
            $user = User::create([
                'name' => 'Admin',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin'),
            ]);
            $service->create($user, 'admin', 'admin', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'admin.png']);
        }

        /* create($user, $password, $role = 'member', $sendEmail = true) */

        if (!User::where('name', 'Member')->first()) {
            $user = User::create([
                'name' => 'Member',
                'email' => 'member@member.com',
                'password' => bcrypt('member'),
            ]);
            $service->create($user, 'admin', 'admin', false);
            $activation = UserMeta::where('user_id', $user->id)->update(['is_active' => 1]);
            $avatar = UserMeta::where('user_id', $user->id)->update(['avatar_file_name' => 'member.jpg']);
        }

    }
}
