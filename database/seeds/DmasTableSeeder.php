<?php  

use App\Models\Dma;
use Illuminate\Database\Seeder;

class DmasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Mengisi sampel data pertama 
        Dma::create([
			'zona_id' => '1',
			'kode' => '01',
			'name' => 'Gandrung',
			'polygon' => 'laravel',
        ]);

        Dma::create([
			'zona_id' => '2',
			'kode' => '02',
			'name' => 'Asri',
			'polygon' => 'laravel1',
         ]);

        Dma::create([
			'zona_id' => '3',
			'kode' => '03',
			'name' => 'Graha Bukit Raya',
			'polygon' => 'laravel2',
         ]);
    }
}
