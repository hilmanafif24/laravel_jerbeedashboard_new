<?php
use App\Models\Location;
use Illuminate\Database\Seeder;

class LocationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Location::create([
            'id' => '1',
            'name' => 'Cisarua1'

        ]);

        Location::create([
            'id' => '2',
            'name' => 'Ciwidey'
        ]);
        Location::create([
            'id' => '3',
            'name' => 'Cisarua3'
        ]);
    }
}
