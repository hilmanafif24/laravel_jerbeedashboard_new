<?php

use Illuminate\Database\Seeder;
use App\Models\Spam;

class SpamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Spam::create([
          'id' => '1',
          'kode' => '01',
          'name' => 'Cisarua',
          'polygon'=>''
      ]);
      Spam::create([
          'id' => '2',
          'kode' => '02',
          'name' => 'Ciwidey',
          'polygon'=>''
      ]);
      Spam::create([
          'id' => '3',
          'kode' => '03',
          'name' => 'Padalarang',
          'polygon'=>''
      ]);
    }
}
