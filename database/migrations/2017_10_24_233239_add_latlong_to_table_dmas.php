<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatlongToTableDmas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('dmas', function(Blueprint $table) {
      $table->double('lat')->nullable();
      $table->double('long')->nullable();
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('dmas', function(Blueprint $table) {
      $table->dropColumn('lat');
      $table->dropColumn('long');
      });
    }
}
