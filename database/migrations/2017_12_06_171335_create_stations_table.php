<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stations', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('brand_id');
			$table->integer('location_id');
			$table->string('name');
			$table->dateTime('instalation_date');
			$table->string('lat');
			$table->string('long');
			$table->integer('v_spam_kode_id');
			$table->string('kode');
			$table->string('chart_url');
			$table->string('data_url');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stations');
    }
}
