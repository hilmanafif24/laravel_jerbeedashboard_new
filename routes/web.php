<?php

use Illuminate\Support\Facades\Route;

// Non protected/public routes
Route::get('/', 'WelcomeController@index');
Route::get('pdashboard', 'DashboardController@index');
Route::get('pdashboard/detail', 'DashboardController@detail');
Route::get('pdashboard/detail_nrw', 'DashboardController@detail_nrw');
// Login Logout
Route::get('login', 'Auth\LoginController@showLoginForm');
Route::post('login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout');
// Password Reset Routes...
Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
Route::post('password/reset', 'Auth\ResetPasswordController@reset');
// Regisration
Route::get('register', 'Auth\RegisterController@showRegistrationForm');
Route::post('register', 'Auth\RegisterController@register');
Route::get('activate', 'Auth\ActivateController@showActivate');
Route::get('activate/send-token', 'Auth\ActivateController@sendToken');
Route::get('activate/token/{token}', 'Auth\ActivateController@activate');

// HARUS login / Authenticated Routes
Route::group(['middleware' => ['auth', 'active']], function () {
    // User stuff
    Route::get('/users/switch-back', 'Admin\UserController@switchUserBack');
    Route::group(['prefix' => 'user', 'namespace' => 'User'], function () {
        Route::get('settings', 'SettingsController@settings');
        Route::post('settings', 'SettingsController@update');
        Route::post('editmeta/{id}', 'SettingsController@updatemeta');
        Route::get('password', 'PasswordController@password');
        Route::post('password', 'PasswordController@update');
    });
    // Dashboard
    Route::get('/dashboard', 'PagesController@dashboard');

    // Team
    Route::get('team/{name}', 'TeamController@showByName');
    Route::resource('teams', 'TeamController', ['except' => ['show']]);
    Route::post('teams/search', 'TeamController@search');
    Route::post('teams/{id}/invite', 'TeamController@inviteMember');
    Route::get('teams/{id}/remove/{userId}', 'TeamController@removeMember');


    // HARUS login, HARUS admin atau member
    Route::group(['middleware' => 'roles:admin|member'], function () {
        Route::resource('contents', 'ContentsController');
        Route::post('contents/search', [
            'as' => 'contents.search',
            'uses' => 'ContentsController@search'
        ]);
    });

    // HARUS login, HARUS admin
    Route::group(['middleware' => 'roles:admin'], function () {
      // Organization & References
      Route::resource('companies', 'CompaniesController');
      Route::post('companies/search', [
          'as' => 'companies.search',
          'uses' => 'CompaniesController@search'
      ]);

      // CMS
      Route::resource('topics', 'TopicsController');
      Route::post('topics/search', [
          'as' => 'topics.search',
          'uses' => 'TopicsController@search'
      ]);
      Route::resource('categories', 'CategoriesController');
      Route::post('categories/search', [
          'as' => 'categories.search',
          'uses' => 'CategoriesController@search'
      ]);
      Route::resource('offlinewriters', 'OfflineWritersController');
      Route::post('offlinewriters/search', [
          'as' => 'offlinewriters.search',
          'uses' => 'OfflineWritersController@search'
      ]);
      Route::resource('comments', 'CommentsController');
      Route::post('comments/search', [
          'as' => 'comments.search',
          'uses' => 'CommentsController@search'
      ]);
      // Log System
      Route::resource('logsystems', 'LogsystemsController');
      Route::post('logsystems/search', [
          'as' => 'logsystems.search',
          'uses' => 'LogsystemsController@search'
      ]);
      // Spams
      Route::resource('spams', 'SpamsController');
      Route::post('spams/search', [
          'as' => 'spams.search',
          'uses' => 'SpamsController@search'
      ]);

      // Dma
      Route::resource('dmas', 'DmasController');
      Route::post('dmas/search', [
      'as' => 'dmas.search',
      'uses' => 'DmasController@search'
      ]);
      Route::post('dmas/assign', [
      'as' => 'dmas.assign',
      'uses' => 'DmasController@assign'
      ]);

      // Zona
      Route::resource('zonas', 'ZonasController');
      Route::post('zonas/search', [
          'as' => 'zonas.search',
          'uses' => 'ZonasController@search'
      ]);

      // Brand
      Route::resource('brands', 'BrandsController');
      Route::post('brands/search', [
          'as' => 'brands.search',
          'uses' => 'BrandsController@search'
      ]);
      // Location
      Route::resource('locations', 'LocationsController');
      Route::post('locations/search', [
          'as' => 'locations.search',
          'uses' => 'LocationsController@search'
      ]);

      // Station
      Route::resource('stations', 'StationsController');
      Route::post('stations/search', [
          'as' => 'stations.search',
          'uses' => 'StationsController@search'
      ]);

      // Device
      Route::resource('devices', 'DevicesController');
      Route::post('devices/search', [
          'as' => 'devices.search',
          'uses' => 'DevicesController@search'
      ]);

      // Grafik & Statistik
      Route::get('waterbalance', [
          'as' => 'nrws.waterbalance',
          'uses' => 'NrwsController@waterbalance'
      ]);
      Route::get('chartmeasurement', [
          'as' => 'nrws.chartmeasurement',
          'uses' => 'NrwsController@chartmeasurement'
      ]);


    });

    // HARUS login, HARUS admin dan controllernya HARUS di /admin
    Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
        // User
        Route::resource('users', 'UserController', ['except' => ['create', 'show']]);
        Route::post('users/search', 'UserController@search');
        Route::get('users/search', 'UserController@index');
        Route::get('users/invite', 'UserController@getInvite');
        Route::get('users/switch/{id}', 'UserController@switchToUser');
        Route::post('users/invite', 'UserController@postInvite');
        // Roles
        Route::resource('roles', 'RoleController', ['except' => ['show']]);
        Route::post('roles/search', 'RoleController@search');
        Route::get('roles/search', 'RoleController@index');
    });
});
